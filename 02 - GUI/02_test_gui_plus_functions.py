import sys
from functools import partial #For argument through buttons
from PyQt5.QtCore import Qt,QTimer
from PyQt5.QtWidgets import (QApplication, QMessageBox, QCheckBox, QGridLayout, QGroupBox, QLineEdit,
        QMenu, QPushButton, QRadioButton, QVBoxLayout, QWidget,QComboBox,QLabel)
import serial.tools.list_ports
import numpy as np
import pyqtgraph as pg
from random import randint
import Py_P_Pump
import propar
import time

class Window(QWidget):
    #Initilization Function
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        
        #Group box for parameters
        comBox = QGroupBox("COM Ports")
        comGrid = QGridLayout()

        refreshButton = QPushButton('Refresh COM Ports', self)
        refreshButton.clicked.connect(self.refreshMethod)

        comGrid.addWidget(refreshButton,1,0,1,4)

        comPortsCPLabel = QLabel("Continuous Phase Pump", self) 
        self.comPortsCP = QComboBox()
        self.stateCPLabel = QLabel("Disconnected",self)

        comGrid.addWidget(comPortsCPLabel,2,0)
        comGrid.addWidget(self.comPortsCP,2,1)
        comGrid.addWidget(self.stateCPLabel,2,2,1,2)

        comPortsDPLabel = QLabel("Dispersed Phase Pump", self)
        self.comPortsDP = QComboBox()
        self.stateDPLabel = QLabel("Disconnected",self)

        comGrid.addWidget(comPortsDPLabel,3,0)
        comGrid.addWidget(self.comPortsDP,3,1)        
        comGrid.addWidget(self.stateDPLabel,3,2,1,2)

        comPortsFMLabel = QLabel("Flowmeter", self)
        self.stateFMLabel = QLabel("Disconnected", self)
        self.comPortsFM = QComboBox() #BronkHorst FlowMeter
        
        comGrid.addWidget(comPortsFMLabel,4,0)
        comGrid.addWidget(self.comPortsFM,4,1)        
        comGrid.addWidget(self.stateFMLabel,4,2,1,2)

        connectButton = QPushButton('Connect to instruments', self)
        connectButton.clicked.connect(self.connectMethod)  
        disconnectButton = QPushButton('Disconnect from instruments', self)
        disconnectButton.clicked.connect(self.disconnectMethod)
        
        comGrid.addWidget(connectButton,5,0,1,2)
        comGrid.addWidget(disconnectButton,5,2,1,2)

        self.testPump = QComboBox()
        self.testPressure = QComboBox()
        self.testPressure.addItems(['0','100','200','400','600','800','1000'])
        testPressureLabel = QLabel("mPa")
        testPressureButton = QPushButton("Test Pump", self)
        testPressureButton.clicked.connect(self.testPressureMethod)

        comGrid.addWidget(self.testPump,6,0)
        comGrid.addWidget(self.testPressure,6,1)
        comGrid.addWidget(testPressureLabel,6,2)
        comGrid.addWidget(testPressureButton,6,3)

        self.testFlow = QComboBox()
        testFlowResult = QLabel("No data")
        testFlowLabel = QLabel("uL/min")
        testFlowButton = QPushButton("Test Flow Measure", self)
        testFlowButton.clicked.connect(self.testFlowmeterMethod)

        comGrid.addWidget(self.testFlow,7,0)
        comGrid.addWidget(testFlowResult,7,1)
        comGrid.addWidget(testFlowLabel,7,2)
        comGrid.addWidget(testFlowButton,7,3)
       
        comBox.setMaximumWidth(500)
        comBox.setLayout(comGrid)

        #Parameters group bax

        paramBox = QGroupBox("Parameters")
        paramGrid = QGridLayout()

        self.csvCheckbox = QCheckBox("Save as CSV")
        self.csvFilenameLabel = QLabel("File name:")
        self.csvFilename = QLineEdit()

        paramGrid.addWidget(self.csvFilenameLabel,0,0)
        paramGrid.addWidget(self.csvFilename,0,1,1,2)
        paramGrid.addWidget(self.csvCheckbox,0,3)

        self.freq = QLineEdit()
        freqLabel = QLabel("Sampling Frequency")
        freqLabelHz = QLabel("Hz") 

        paramGrid.addWidget(freqLabel,1,0)
        paramGrid.addWidget(self.freq,1,1)
        paramGrid.addWidget(freqLabelHz,1,2)

        measureTime = QLineEdit()
        measureTimeLabel = QLabel("Plot width")
        measureTimeLabelUnit = QLabel("s")
        self.timeCheckbox = QCheckBox("Continuous")

        paramGrid.addWidget(measureTimeLabel,2,0)
        paramGrid.addWidget(measureTime,2,1)
        paramGrid.addWidget(measureTimeLabelUnit,2,2)
        paramGrid.addWidget(self.timeCheckbox,2,3)

        
        runButton = QPushButton("Run Measurement", self)
        runButton.clicked.connect(self.startMethod)
        stopButton = QPushButton("Stop measurement",self)
        stopButton.clicked.connect(self.stopMethod)
        clearButton = QPushButton("ClearPlot",self)
        clearButton.clicked.connect(self.clearMethod)
       
        paramGrid.addWidget(runButton,3,0,1,1)
        paramGrid.addWidget(stopButton,3,1,1,2)
        paramGrid.addWidget(clearButton,3,3)
        


        self.pressureContinuous = QLineEdit()
        self.pressureDispersed = QLineEdit()
        presCLabel = QLabel('Pressure Continuous Phase (mPa)')
        presDLabel = QLabel('Pressure Dispersed Phase (mPa)')

        paramGrid.addWidget(presCLabel,5,0,1,2)
        paramGrid.addWidget(self.pressureContinuous,5,3)
        paramGrid.addWidget(presDLabel,6,0,1,2)
        paramGrid.addWidget(self.pressureDispersed,6,3)

        pressButton = QPushButton("Change pressure", self)
        pressButton.clicked.connect(self.setPressMethod)

        paramGrid.addWidget(pressButton,7,0,1,4)

        flowCmd = QLineEdit()
        flowCmdLabel = QLabel("Continuous Flowrate Setpoint (uL/min)")
        flowButton = QPushButton("Change flowrate")

        paramGrid.addWidget(flowCmdLabel,8,0,1,2)
        paramGrid.addWidget(flowCmd,8,3)
        paramGrid.addWidget(flowButton,9,0,1,4)


        paramBox.setMaximumWidth(500)
        paramBox.setLayout(paramGrid)

        #Group box for plots
        plotBox = QGroupBox("Plots")
        plotGrid = QGridLayout()

        self.graphWidget = pg.PlotWidget()
        self.graphWidget.setMinimumWidth(700)
        self.clearMethod() #Initialize some variables for the plot

        self.pen1 = pg.mkPen(color=(255, 0, 0))
        self.pen2 = pg.mkPen(color=(0, 255, 0))

        self.pressure = self.graphWidget.plotItem
        #self.setpointPressCP
        self.pressure.setLabels(left='Pressure (mPa)')

        ## create a new ViewBox, link the right axis to its coordinate system
        self.flow = pg.ViewBox()
        self.pressure.showAxis('right')
        self.pressure.scene().addItem(self.flow)
        self.pressure.getAxis('right').linkToView(self.flow)
        self.flow.setXLink(self.pressure)
        self.pressure.getAxis('right').setLabel('Flow (uL/min)', color='#ff0000')
        self.flowitem = pg.PlotCurveItem(self.time,self.plotFlow, pen='g')
        
        self.updateViews()
        self.pressure.vb.sigResized.connect(self.updateViews)

        

        plotGrid.addWidget(self.graphWidget,0,0)
        plotBox.setLayout(plotGrid)

        #Grid 
        grid = QGridLayout()
        grid.addWidget(comBox, 0, 0)
        grid.addWidget(paramBox, 1, 0)
        grid.addWidget(plotBox, 0, 1,2,1)
        self.setLayout(grid)

        
        self.setWindowTitle("BiAcoustic")
        self.resize(400, 300)
    
    def updateViews(self):
            ## view has resized; update auxiliary views to match
            #global p1, p2, p3
            self.flow.setGeometry(self.pressure.vb.sceneBoundingRect())
            
            ## need to re-update linked axes since this was called
            ## incorrectly while views had different shapes.
            ## (probably this should be handled in ViewBox.resizeEvent)
            self.flow.linkedViewChanged(self.pressure.vb, self.flow.XAxis)


    def errorPort(self,port):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)

        msg.setText(f"Can't communicate with the port {port}")
        msg.setInformativeText("Check if it's correct or if it's used by another software")
        msg.exec_()

    def clearMethod(self):
        self.startTime = time.time()
        self.time = []
        self.plotPressC = []
        self.plotPressD = []
        self.plotFlow = []
        self.graphWidget.clear()
        
    def startMethod(self):
        self.flowitem = pg.PlotCurveItem(self.time,self.plotFlow, pen='g')
        if self.time == []:
            self.startTime = time.time()
        else:
            if self.endTime != None:
                self.startTime = self.endTime #Get the last time used
        
        self.pressure.plot(self.time,self.plotPressC,pen='b')
        self.pressure.plot(self.time,self.plotPressD,pen='r')
        
        self.flow.addItem(self.flowitem)

        self.timer = QTimer()
        try:
            print(int(1000/int(self.freq.text())))
            self.timer.setInterval(int(1000/int(self.freq.text())))
            self.timer.timeout.connect(self.update_plot_data)
            self.timer.start()
        except:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)

            msg.setText(f"Incorrect frequency")
            msg.setInformativeText("Please use only numbers")
            msg.exec_()

    def update_plot_data(self):
        self.endTime = time.time()
        if self.endTime>self.startTime:
            self.time.append(self.endTime-self.startTime)
            #self.plotPressC = self.plotPressC[1:]  # Remove the first
            self.plotPressC.append(randint(20,40))  # Add a new random value.

            self.plotFlow.append( randint(20,40))  # Add a new random value.

            self.graphWidget.clear()

            self.graphWidget.plot(self.time, self.plotPressC,pen=self.pen1)  # Update the data.
            self.flow.addItem(self.flowItem)
            self.updateViews()
            self.pressure.vb.sigResized.connect(self.updateViews)
    
    def stopMethod(self):
        self.timer.stop()

    def refreshMethod(self):
        print("Refresh")
        self.comPortsCP.clear()
        self.comPortsDP.clear()
        self.comPortsFM.clear()
        self.testPump.clear()
        self.testFlow.clear()
        ports = serial.tools.list_ports.comports()

        for port, desc, hwid in sorted(ports):
                print("{}: {} [{}]".format(port, desc, hwid))
                self.comPortsCP.addItem(port)
                self.comPortsDP.addItem(port)
                self.comPortsFM.addItem(port)
                self.testPump.addItem(port)
                self.testFlow.addItem(port)

    def connectMethod(self):
        try:
            self.continuousPump = Py_P_Pump.P_pump(self.comPortsCP.currentText(), name='Continuous Phase Pump', pump_id=0, verbose=True)
            self.stateCPLabel.setText("Connected")
        except:
            self.errorPort(self.comPortsCP.currentText())
        try:
            self.dispersedPump = Py_P_Pump.P_pump(self.comPortsDP.currentText(), name='Dispersed Phase Pump', pump_id=1, verbose=True)
            self.stateDPLabel.setText("Connected")
        except:
            self.errorPort(self.comPortsDP.currentText())
        try:
            self.flowMeter = propar.instrument(self.comPortsFM.currentText())
            self.stateFMLabel.setText("Connected")
        except:
           self.errorPort(self.comPortsFM.currentText())
    
    def disconnectMethod(self):
        try:
            self.continuousPump.set_idle()
            self.dispersedPump.set_idle()
        except:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)

            msg.setText(f"Trouble Disconnecting the Pumps")
            msg.setInformativeText("Please restart the script after saving your data")
            msg.exec_()
        
        self.continuousPump = None
        self.dispersedPump = None

        self.stateCPLabel.setText("Disconnected")
        self.stateDPLabel.setText("Disconnected")
        self.stateFMLabel.setText("Disconnected")
    
    def testPressureMethod(self):
        try:
            testPumpDevice = Py_P_Pump.P_pump(self.testPump.currentText(), name='Continuous Phase Pump', pump_id=0, verbose=True)
            testPumpDevice.set_pressure(int(self.testPressure.currentText()), time='00:00:00:10')
        except:
            self.errorPort(self.testPump.currentText())
        del testPumpDevice
    
    def testFlowmeterMethod(self):
        try:
            testFlowMeter = propar.instrument(self.testFlow.currentText())
            flow_read = testFlowMeter.read(33, 0, propar.PP_TYPE_FLOAT)
            print(flow_read)
            self.testFlowResult.setText(str(flow_read))
            testFlowMeter = None
        except:
            self.errorPort(self.testFlow.currentText())
    
    def setPressMethod(self):
        try:
            self.dispersedPump.set_pressure(int(self.pressureDispersed.text()), time='00:00:00:00')
        except:
            self.errorPort(self.comPortsDP.currentText())
        try:
            self.continuousPump.set_pressure(int(self.pressureContinuous.text()), time='00:00:00:00')
        except:
            self.errorPort(self.comPortsCP.currentText())
        
        


if __name__ == '__main__':
    app = QApplication(sys.argv)
    clock = Window()
    clock.show()
    sys.exit(app.exec_())
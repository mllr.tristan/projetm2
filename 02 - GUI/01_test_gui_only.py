import sys
from functools import partial #For argument through buttons
from PyQt5.QtCore import Qt,QTimer
from PyQt5.QtWidgets import (QApplication, QCheckBox, QGridLayout, QGroupBox, QLineEdit,
        QMenu, QPushButton, QRadioButton, QVBoxLayout, QWidget,QComboBox,QLabel)
import serial.tools.list_ports
import numpy as np
import pyqtgraph as pg
from random import randint

class Window(QWidget):
    #Initilization Function
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        #Group box for parameters
        comBox = QGroupBox("Ports COM")
        comGrid = QGridLayout()

        refreshButton = QPushButton('Refresh COM Ports', self)
        connectButton = QPushButton('Connect to instruments', self)

        comPortsCPLabel = QLabel("Continuous Phase Pump", self) 
        comPortsDPLabel = QLabel("Dispersed Phase Pump", self) 
        comPortsFMLabel = QLabel("Flowmeter", self)

        self.comPortsCP = QComboBox()
        self.comPortsDP = QComboBox()
        self.comPortsFM = QComboBox() #BronkHorst FlowMeter
        
        self.testPump = QComboBox()
        testPressure = QComboBox()
        testPressure.addItems(['0','100','200','400','600','800','1000'])
        testPressureLabel = QLabel("mPa")
        testPressureButton = QPushButton("Test Pump", self)

        self.testFlow = QComboBox()
        testFlowButton = QPushButton("Test Flow Measure", self)
        testFlowResult = QLabel("No data")
        testFlowLabel = QLabel("uL/min")


        refreshButton.clicked.connect(self.refreshMethod)
        connectButton.clicked.connect(self.refreshMethod)  
        testPressureButton.clicked.connect(self.refreshMethod)        
        
        #1st Line
        comGrid.addWidget(refreshButton,1,0,1,4)
        #2nd Line
        comGrid.addWidget(comPortsCPLabel,2,0)
        comGrid.addWidget(self.comPortsCP,2,1)
        #3rd Line
        comGrid.addWidget(comPortsDPLabel,3,0)
        comGrid.addWidget(self.comPortsDP,3,1)
        #4th Line
        comGrid.addWidget(comPortsFMLabel,4,0)
        comGrid.addWidget(self.comPortsFM,4,1)
        #5th Line
        comGrid.addWidget(connectButton,5,0,1,4)
        #6th Line
        comGrid.addWidget(self.testPump,6,0)
        comGrid.addWidget(testPressure,6,1)
        comGrid.addWidget(testPressureLabel,6,2)
        comGrid.addWidget(testPressureButton,6,3)
        #7th Line
        comGrid.addWidget(self.testFlow,7,0)
        comGrid.addWidget(testFlowResult,7,1)
        comGrid.addWidget(testFlowLabel,7,2)
        comGrid.addWidget(testFlowButton,7,3)
       
        comBox.setMaximumWidth(500)
        comBox.setLayout(comGrid)

        paramBox = QGroupBox("Parameters")
        paramGrid = QGridLayout()

        self.csvCheckbox = QCheckBox("Save as CSV")
        self.csvFilenameLabel = QLabel("File name:")
        self.csvFilename = QLineEdit()
        paramGrid.addWidget(self.csvFilenameLabel,0,0)
        paramGrid.addWidget(self.csvFilename,0,1,1,2)
        paramGrid.addWidget(self.csvCheckbox,0,3)

        freq = QLineEdit()
        #freq.setFixedWidth(100)
        freqLabel = QLabel("Sampling Frequency")
        freqLabelHz = QLabel("Hz") 

        paramGrid.addWidget(freqLabel,1,0)
        paramGrid.addWidget(freq,1,1)
        paramGrid.addWidget(freqLabelHz,1,2)

        measureTime = QLineEdit()
        measureTimeLabel = QLabel("Plot width")
        measureTimeLabelUnit = QLabel("s")

        paramGrid.addWidget(measureTimeLabel,2,0)
        paramGrid.addWidget(measureTime,2,1)
        paramGrid.addWidget(measureTimeLabelUnit,2,2)

        
        runButton = QPushButton("Run Measurement", self)
        stopButton = QPushButton("Stop measurement",self)
        paramGrid.addWidget(runButton,3,0,1,4)
        paramGrid.addWidget(stopButton,4,0,1,4)

        pressureContinuous = QLineEdit()
        pressureDispersed = QLineEdit()
        presCLabel = QLabel('Pressure Continuous Phase (mPa)')
        presDLabel = QLabel('Pressure Dispersed Phase (mPa)')

        paramGrid.addWidget(presCLabel,5,0,1,2)
        paramGrid.addWidget(pressureContinuous,5,3)
        paramGrid.addWidget(presDLabel,6,0,1,2)
        paramGrid.addWidget(pressureDispersed,6,3)

        presButton = QPushButton("Change pressure", self)
        paramGrid.addWidget(presButton,7,0,1,4)

        flowCmd = QLineEdit()
        flowCmdLabel = QLabel("Continuous Flowrate Setpoint (uL/min)")
        flowButton = QPushButton("Change flowrate")

        paramGrid.addWidget(flowCmdLabel,8,0,1,2)
        paramGrid.addWidget(flowCmd,8,3)
        paramGrid.addWidget(flowButton,9,0,1,4)


        paramBox.setMaximumWidth(500)
        paramBox.setLayout(paramGrid)

        #Group box for plots
        plotBox = QGroupBox("Plots")
        plotGrid = QGridLayout()

        self.graphWidget = pg.PlotWidget()
        self.graphWidget.setMinimumWidth(700)

        self.temp1 = self.graphWidget.plotItem
        self.temp1.setLabels(left='axis 1')

        ## create a new ViewBox, link the right axis to its coordinate system
        self.temp2 = pg.ViewBox()
        self.temp1.showAxis('right')
        self.temp1.scene().addItem(self.temp2)
        self.temp1.getAxis('right').linkToView(self.temp2)
        self.temp2.setXLink(self.temp1)
        self.temp1.getAxis('right').setLabel('axis2', color='#ff0000')

        def updateViews():
            ## view has resized; update auxiliary views to match
            #global p1, p2, p3
            self.temp2.setGeometry(self.temp1.vb.sceneBoundingRect())
            
            ## need to re-update linked axes since this was called
            ## incorrectly while views had different shapes.
            ## (probably this should be handled in ViewBox.resizeEvent)
            self.temp2.linkedViewChanged(self.temp1.vb, self.temp2.XAxis)

        updateViews()
        self.temp1.vb.sigResized.connect(updateViews)

        self.hour = [2,4,6,8,10,12,14,16,18,20]
        self.temp1.plot(self.hour,[1,2,4,8,16,32,4,8,16,32])
        self.temp2.addItem(pg.PlotCurveItem(self.hour,[10,80,40,20,10,20,40,80,40,20], pen='r'))

        #self.temperature = [30,32,34,32,33,31,29,32,35,45]
        #self.temp2 = [10,11,20,12,14,12,34,21,22,11]

        self.timer = QTimer()
        self.timer.setInterval(500)
        self.timer.timeout.connect(self.update_plot_data)
        #self.timer.start()

        # plot data: x, y values
        #self.graphWidget.plot(self.hour, self.temperature)
        #self.graphWidget.plot(self.hour, self.temp2)

        plotGrid.addWidget(self.graphWidget,0,0)
        plotBox.setLayout(plotGrid)

        

        #Grid 
        grid = QGridLayout()
        grid.addWidget(comBox, 0, 0)
        grid.addWidget(paramBox, 1, 0)
        grid.addWidget(plotBox, 0, 1,2,1)
        self.setLayout(grid)

        
        self.setWindowTitle("BiAcoustic")
        self.resize(400, 300)
    
    def update_plot_data(self):
        pen1 = pg.mkPen(color=(255, 0, 0))
        pen2 = pg.mkPen(color=(0, 255, 0))

        self.temp2 = self.temp2[1:]  # Remove the first
        self.temp2.append( randint(20,40))  # Add a new random value.
        self.graphWidget.clear()

        self.graphWidget.plot(self.hour, self.temp2,pen=pen1)  # Update the data.
        self.graphWidget.plot(self.hour, self.temperature,pen = pen2)  # Update the data.
    
    def refreshMethod(self):
        print("Refresh")
        self.comPortsCP.clear()
        self.comPortsDP.clear()
        self.comPortsFM.clear()
        self.testPump.clear()
        self.testFlow.clear()
        ports = serial.tools.list_ports.comports()

        for port, desc, hwid in sorted(ports):
                print("{}: {} [{}]".format(port, desc, hwid))
                self.comPortsCP.addItem(port)
                self.comPortsDP.addItem(port)
                self.comPortsFM.addItem(port)
                self.testPump.addItem(port)
                self.testFlow.addItem(port)
                


        


if __name__ == '__main__':
    app = QApplication(sys.argv)
    clock = Window()
    clock.show()
    sys.exit(app.exec_())
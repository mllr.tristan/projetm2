from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QCheckBox, QComboBox, QFileDialog, QGridLayout, QGroupBox, QLabel,QWidget
from PyQt5.QtGui import QImage
import cv2, imutils
import numpy as np
import torch
import statistics

#Program based on https://pyshine.com/Make-GUI-for-OpenCv-And-PyQt5/

class Window(QWidget):
	#Initilization Function
	def __init__(self, parent=None):
		super(Window, self).__init__(parent)
		self.setWindowTitle("BiAcoustic")
		self.resize(400, 300)
		
		self.conf_value_now = 70
		self.sizeChamber = 1100

		#Loading the pre-trained  YOLO model
		self.model = torch.hub.load('yolov5','custom',path='best500s',source='local',device='cpu')  # or yolov5m, yolov5l, yolov5x, custom

		
		self.filename = None # Will hold the image address location
		self.tmp = None # Will hold the temporary image for display
		

		self.grid = QGridLayout()

		self.paramBox = QGroupBox("Parameters")
		self.paramGrid = QGridLayout()

		self.label = QtWidgets.QLabel()
		self.label.setText("")
		#self.label.setPixmap(QtGui.QPixmap("pic7.bmp"))
		self.label.setObjectName("label")
		
		self.saveButton = QtWidgets.QPushButton("Save Image")
		self.openButton = QtWidgets.QPushButton("Open Image")


		self.confLabel = QtWidgets.QLabel("Confidence Threshold")
		self.confValSpinbox = QtWidgets.QSpinBox()
		self.confValSpinbox.setKeyboardTracking(False)

		self.confValSpinbox.setRange(0,90)
		self.confValSpinbox.setSingleStep(5)
		self.confValSpinbox.setValue = 70

		self.paramGrid.addWidget(self.openButton, 0, 0,1,2)	
		self.paramGrid.addWidget(self.saveButton, 0, 2,1,2)

		self.filterBox = QGroupBox("Image Processing")
		self.filterGrid = QGridLayout()
		self.filterGrid.addWidget(self.confLabel,7,0)
		self.filterGrid.addWidget(self.confValSpinbox,7,1)


		self.filterBox.setLayout(self.filterGrid)


		self.cursorsBox = QGroupBox("Cursors Parameters")
		self.cursorsGrid = QGridLayout()

		self.xMarkers = 300
		self.yMarker1 = 200
		self.yMarker2 = 400

		self.xMarkersSlider = QtWidgets.QSlider()
		self.xMarkersSlider.setRange(0,2000)
		self.xMarkersSlider.setOrientation(QtCore.Qt.Horizontal)
		self.xMarkersSlider.setValue(self.xMarkers)
		self.xMarkersLabel = QtWidgets.QLabel("Horizontal Position")
		self.xMarkersValSpinbox = QtWidgets.QSpinBox()

		self.yMarker1Slider = QtWidgets.QSlider()
		self.yMarker1Slider.setRange(0,2000)
		self.yMarker1Slider.setOrientation(QtCore.Qt.Horizontal)
		self.yMarker1Slider.setValue(self.yMarker1)
		self.yMarker1Label = QtWidgets.QLabel("Vertical Position Green")
		self.yMarker1ValSpinbox = QtWidgets.QSpinBox()

		self.yMarker2Slider = QtWidgets.QSlider()
		self.yMarker2Slider.setRange(0,2000)
		self.yMarker2Slider.setOrientation(QtCore.Qt.Horizontal)
		self.yMarker2Slider.setValue(self.yMarker2)
		self.yMarker2Label = QtWidgets.QLabel("Vertical Position Red")
		self.yMarker2ValSpinbox = QtWidgets.QSpinBox()
		
		self.sizeChamberLabel = QtWidgets.QLabel("Chamber Size")
		self.sizeChamberBox = QtWidgets.QSpinBox(self)
		self.sizeChamberBox.setKeyboardTracking(False)
		self.sizeChamberBox.setRange(0, 10000)
		self.sizeChamberBox.setSingleStep(100)
		self.sizeChamberBox.setSuffix(' um')

		self.cursorsGrid.addWidget(self.xMarkersLabel,0,0)
		self.cursorsGrid.addWidget(self.xMarkersValSpinbox,0,1)
		self.cursorsGrid.addWidget(self.xMarkersSlider, 0, 2, 1, 2)

		self.cursorsGrid.addWidget(self.yMarker1Label,1,0)
		self.cursorsGrid.addWidget(self.yMarker1ValSpinbox,1,1)
		self.cursorsGrid.addWidget(self.yMarker1Slider, 1, 2, 1, 2)

		self.cursorsGrid.addWidget(self.yMarker2Label,2,0)
		self.cursorsGrid.addWidget(self.yMarker2ValSpinbox,2,1)
		self.cursorsGrid.addWidget(self.yMarker2Slider, 2, 2, 1, 2)

		self.cursorsGrid.addWidget(self.sizeChamberLabel, 3, 0, 1, 2)
		self.cursorsGrid.addWidget(self.sizeChamberBox, 3, 2)
		
		self.cursorsBox.setLayout(self.cursorsGrid)

		self.measureBox = QGroupBox("Measurement")
		self.measureGrid = QGridLayout()

		self.drawMeasureCheck = QCheckBox()
		self.drawMeasureLabel = QLabel("Draw measurements on the image")

		self.meanRadiusLabel = QLabel("Mean radius of Bubbles: ")
		self.meanRadius = QLabel("TBD")
		self.meanRadiusUnit = QLabel("um")

		self.medianRadiusLabel = QLabel("Median radius of Bubbles: ")
		self.medianRadius = QLabel("TBD")
		self.medianRadiusUnit = QLabel("um")

		self.firstPercentileLabel = QLabel("1st Percentile")
		self.firstPercentile = QLabel("TBD")
		self.firstPercentileUnit = QLabel("um")

		self.thirdPercentileLabel = QLabel("3rd Percentile")
		self.thirdPercentile = QLabel("TBD")
		self.thirdPercentileUnit = QLabel("um")

		self.maxRadiusLabel = QLabel("Maximum Radius")
		self.maxRadius = QLabel("TBD")
		self.maxRadiusUnit = QLabel("um")

		self.minRadiusLabel = QLabel("Minimum Radius")
		self.minRadius = QLabel("TBD")
		self.minRadiusUnit= QLabel("um")

		self.measureGrid.addWidget(self.meanRadiusLabel,0,0)
		self.measureGrid.addWidget(self.meanRadius, 0, 1)
		self.measureGrid.addWidget(self.meanRadiusUnit, 0,2)

		self.measureGrid.addWidget(self.medianRadiusLabel,1,0)
		self.measureGrid.addWidget(self.medianRadius,1,1)
		self.measureGrid.addWidget(self.medianRadiusUnit,1,2)

		self.measureGrid.addWidget(self.firstPercentileLabel,2,0)
		self.measureGrid.addWidget(self.firstPercentile,2,1)
		self.measureGrid.addWidget(self.firstPercentileUnit,2,2)

		self.measureGrid.addWidget(self.thirdPercentileLabel,3,0)
		self.measureGrid.addWidget(self.thirdPercentile,3,1)
		self.measureGrid.addWidget(self.thirdPercentileUnit,3,2)

		self.measureGrid.addWidget(self.minRadiusLabel,4,0)
		self.measureGrid.addWidget(self.minRadius,4,1)
		self.measureGrid.addWidget(self.minRadiusUnit,4,2)

		self.measureGrid.addWidget(self.maxRadiusLabel,5,0)
		self.measureGrid.addWidget(self.maxRadius,5,1)
		self.measureGrid.addWidget(self.maxRadiusUnit,5,2)

		self.measureBox.setLayout(self.measureGrid)


		self.paramGrid.addWidget(self.filterBox,2,0,1,4)
		self.paramGrid.addWidget(self.cursorsBox,3,0,1,4)
		self.paramGrid.addWidget(self.measureBox,4,0,1,4)

		self.paramBox.setLayout(self.paramGrid)
		self.paramBox.setMinimumWidth(500)
		
		self.grid.addWidget(self.paramBox, 0,0)

		imgBox = QGroupBox("Image")
		imgGrid = QGridLayout()

		spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
		imgGrid.addWidget(self.label,1,1,1,1)
		imgBox.setLayout(imgGrid)
		self.grid.addWidget(imgBox,0,1)

		self.setLayout(self.grid)


		self.confValSpinbox.valueChanged.connect(self.conf_value)

		self.openButton.clicked.connect(self.loadImage)
		self.saveButton.clicked.connect(self.savePhoto)

		self.xMarkersSlider.valueChanged['int'].connect(self.xMarkers_value)
		self.yMarker1Slider.valueChanged['int'].connect(self.yMarker1_value)
		self.yMarker2Slider.valueChanged['int'].connect(self.yMarker2_value)
		self.sizeChamberBox.valueChanged.connect(self.sizeChamber_value)

		self.loadImageDefault()
		self.conf_value(self.conf_value_now)
		self.xMarkers_value(self.xMarkers)
		self.yMarker1_value(self.yMarker1)
		self.yMarker2_value(self.yMarker2)
		self.sizeChamber_value(self.sizeChamber)
		self.update()

	def loadImageDefault(self):
		""" This function will load the user selected image
			and set it to label using the setPhoto function
		"""
		self.filename = "0.png"
		self.image = cv2.imread(self.filename)
		self.setPhoto(self.image)

		#Inference + Bounding box in lists
		self.results = self.model(self.filename, size=1280) # This is where YOLO is working
		self.results = self.results.xyxy[0].tolist()

		self.update()
		#print(len(self.image.shape))

	
	def loadImage(self):
		""" This function will load the user selected image
			and set it to label using the setPhoto function
		"""
		self.filename = QFileDialog.getOpenFileName(filter="Image (*.*)")[0]
		self.image = cv2.imread(self.filename)
		self.setPhoto(self.image)


		#Inference + Bounding box in lists
		self.results = self.model(self.filename) # This is where YOLO is working
		self.results = self.results.xyxy[0].tolist()

		self.update()
		#print(len(self.image.shape))
	
	def setPhoto(self,image):
		""" This function will take image input and resize it 
			only for display purpose and convert it to QImage
			to set at the label.
		"""
		self.tmp = image
		image = imutils.resize(image,width=1024)
		frame = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
		image = QImage(frame, frame.shape[1],frame.shape[0],frame.strides[0],QImage.Format_RGB888)
		self.label.setPixmap(QtGui.QPixmap.fromImage(image))
	
	def conf_value(self,value):
		""" This function will take value from the slider
			for the confidence from 0 to 1
		"""
		self.conf_value_now = value
		self.confValSpinbox.setValue = value
		print('Conf: ',value)
		self.update()
		
		
	
	def xMarkers_value(self,value):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.xMarkers = value
		print('X coord Marker 1: ',self.xMarkers)
		self.xMarkersValSpinbox.blockSignals(True)
		self.xMarkersValSpinbox.setValue(self.xMarkers)
		self.xMarkersSlider.setValue(value)
		self.xMarkersValSpinbox.blockSignals(False)
		self.update()

	def yMarker1_value(self,value):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.yMarker1 = value
		print('Y coord Marker 1: ',self.yMarker1)
		self.yMarker1ValSpinbox.blockSignals(True)
		self.yMarker1ValSpinbox.setValue(self.yMarker1)
		self.yMarker1Slider.setValue(value)
		self.yMarker1ValSpinbox.blockSignals(False)
		self.update()

	def yMarker2_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.yMarker2 = value
		print('Y coord Marker 2: ',self.yMarker2)
		self.yMarker2ValSpinbox.blockSignals(True)
		self.yMarker2ValSpinbox.setValue(self.yMarker2)
		self.yMarker2Slider.setValue(value)
		self.yMarker2ValSpinbox.blockSignals(False)
		if up:
			self.update()
	
	def sizeChamber_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.sizeChamber = value
		print(f'Size chamber: {self.sizeChamber} um')
		self.sizeChamberBox.setValue(value)
		if up:
			self.update()


	
	def drawBoxes(self,img,conf):
		self.boxes = []
		for box in self.results:
			if box[4] > conf/100:
				self.boxes.append([int(box[0]),int(box[1]),int(box[2]),int(box[3])])
				cv2.rectangle(img,(int(box[0]),int(box[1])),(int(box[2]),int(box[3])),(0,0,255),1)
		return img
		
		
	def update(self):
		""" This function will update the photo according to the 
			current values of blur and confness and set it to photo label.
		"""


		img = self.drawBoxes(cv2.imread(self.filename),self.conf_value_now)

		self.xMarkersSlider.setRange(0,img.shape[1])
		self.xMarkersValSpinbox.setRange(0,img.shape[1])
		self.yMarker1Slider.setRange(0,img.shape[0])
		self.yMarker1ValSpinbox.setRange(0,img.shape[0])
		self.yMarker2Slider.setRange(0,img.shape[0])
		self.yMarker2ValSpinbox.setRange(0,img.shape[0])

		cv2.drawMarker(img, (int(self.xMarkers),int(self.yMarker1)), color=(0,255,0), markerType=cv2.MARKER_CROSS, thickness=2)
		cv2.drawMarker(img, (int(self.xMarkers),int(self.yMarker2)), color=(0,0,255), markerType=cv2.MARKER_CROSS, thickness=2)


		#Measurements:
		if self.yMarker1 != self.yMarker2: #Avoid divide by zero
			self.coeffSize = np.abs(self.sizeChamber / (self.yMarker2 - self.yMarker1))
		else:
			print("Move the cursors away from each others")
		#print(self.coeffSize)
		if self.boxes != []:
			radius = []
			#self.circles = np.uint16(np.around(self.circles))
			for box in self.boxes:
				radius.append(np.mean([box[2]-box[0],box[3]-box[1]])*self.coeffSize) #Radius equal to mean of the height and width
			self.meanRadius.setText("{:.3f}".format(np.mean(radius)))
			self.medianRadius.setText("{:.3f}".format(np.median(radius)))
			self.firstPercentile.setText("{:.3f}".format(np.percentile(radius,25)))
			self.thirdPercentile.setText("{:.3f}".format(np.percentile(radius,75)))
			self.maxRadius.setText("{:.3f}".format(np.max(radius)))
			self.minRadius.setText("{:.3f}".format(np.min(radius)))

			#cv2.putText(img,"Mean Radius {:.3f}".format(np.mean(radius)),(0, 50),cv2.FONT_HERSHEY_SIMPLEX,0.0015*img.shape[0],(255, 255, 255),2,cv2.LINE_AA)
		else:
			self.meanRadius.setText("TBD")
			self.medianRadius.setText("TBD")
			self.firstPercentile.setText("TBD")
			self.thirdPercentile.setText("TBD")
			self.maxRadius.setText("TBD")
			self.minRadius.setText("TBD")
		
		self.setPhoto(img)



	
	def savePhoto(self):
		""" This function will save the image"""
		# here provide the output file name
		# lets say we want to save the output as a time stamp
		# uncomment the two lines below
		
		# import time
		# filename = 'Snapshot '+str(time.strftime("%Y-%b-%d at %H.%M.%S %p"))+'.png'
		
		# Or we can give any name such as output.jpg or output.png as well
		# filename = 'Snapshot.png'	
	
		# Or a much better option is to let user decide the location and the extension
          	# using a file dialog.
		
		filename = QFileDialog.getSaveFileName(filter="JPG(*.jpg);;PNG(*.png);;TIFF(*.tiff);;BMP(*.bmp)")[0]
		
		cv2.imwrite(filename,self.tmp)
		print('Image saved as:',self.filename)




	


if __name__ == "__main__":
	import sys
	app = QtWidgets.QApplication(sys.argv)
	MainWindow = Window()
	MainWindow.show()
	sys.exit(app.exec_())
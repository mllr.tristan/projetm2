import Py_P_Pump
import time

#Source of the module :
#https://github.com/linnarsson-lab/Dolomite_P-pump
#Needs to be in the same folder 
#Some comments in the program are copied form the link above

#Find the address where the pump is mounted on your computer when you know the ID of the FTDI chip
#FTDI = USB to SERIAL converter
#addresses = Py_P_Pump.find_address(identifier='Dolomite')

#Configuring the 2 pumps (we will maybe need to let the user decide which one is the blue or red one)
red_pump = Py_P_Pump.P_pump('COM8', name='Red_Pump', pump_id=1, verbose=True)
#blue_pump = Py_P_Pump.P_pump('COM6', name='Blue_Pump', pump_id=2, verbose=True)

#To start pressure control and start pumping with 100 mbar for 10 seconds, run:
red_pump.set_pressure(300, time='00:00:00:00')
#blue_pump.set_pressure(1000, time='00:00:00:00')

time.sleep(3)

red_pump.set_idle()
#blue_pump.set_idle()

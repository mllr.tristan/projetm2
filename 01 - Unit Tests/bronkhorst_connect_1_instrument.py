# Import the propar module
import propar

# Connect to the local instrument, when no settings provided
# defaults to locally connected instrument (address=0x80, baudrate=38400)
el_flow = propar.instrument('COM12')

# The setpoint and measure parameters are available
# as properties, for ease of use.
el_flow.setpoint = 16000
print(el_flow.measure)
el_flow.setpoint = 0

# All parameters can be read using the process and parameter numbers,
# as well as the parameters data type.
#el_flow.read(1, 1, propar.PP_TYPE_INT16)
print(el_flow.read(33, 0, propar.PP_TYPE_FLOAT))

# Most parameters can also be read by their FlowDDE number,
# for example the user tag parameter.
el_flow.writeParameter(115, "Hello World!")
print(el_flow.readParameter(115))
# Import the propar module
import propar

for i in len(range(20)):
    #Go through each COM ports from 0 to 19
    port = f"COM{i}"
    print(port)

    try :
        # Try to connect to the local instrument on the port.
        el_flow = propar.instrument(port)
    
        # Use the get_nodes function of the master of the instrument to get a list of instruments on the network
        nodes = el_flow.master.get_nodes()

        # Display the list of nodes on the port
        for node in nodes:
            print(node)

    except OSError:
        #Try/except is a safeguard in case the program raise an error
        #Without it the script would crash
        #For example if the port is not connected
        print(f"No {port} port connected")
    

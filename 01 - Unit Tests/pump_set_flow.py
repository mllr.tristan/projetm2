import Py_P_Pump
import time

#Source of the module :
#https://github.com/linnarsson-lab/Dolomite_P-pump
#Needs to be in the same folder 
#Some comments in the program are copied form the link above

#Find the address where the pump is mounted on your computer when you know the ID of the FTDI chip
#FTDI = USB to SERIAL converter
addresses = Py_P_Pump.find_address(identifier='Dolomite')

#Configuring the 2 pumps (we will maybe need to let the user decide which one is the blue or red one)
red_pump = Py_P_Pump.P_pump(addresses[0], name='Red_Pump', pump_id=0, verbose=True)
blue_pump = Py_P_Pump.P_pump(addresses[1], name='Blue_Pump', pump_id=1, verbose=True)

#First perform a tare of the pump
red_pump.tare_pump()

#To start flow control with 2ul/s until a stop command is send, run:
red_pump.set_flow(2, unit='ul/s', hold='00:00:00:00')

time.sleep(10)

red_pump.set_idle()
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 20 15:15:35 2021

@author: tmuller
"""

import cv2

import numpy as np

"""
Reading a single image
"""
# Read and display BGR image
bgr = cv2.imread ('pic4.jpg')

cv2.imshow("RGB image", bgr)
cv2.waitKey(0)

gray = cv2.cvtColor(bgr, cv2.COLOR_BGR2GRAY)
cv2.imshow("Gray image", gray)
cv2.waitKey(0)

b,g,r = cv2.split(bgr)
cv2.imshow("Red image", r)
cv2.waitKey(0)
cv2.imshow("Green image", g)
cv2.waitKey(0)
cv2.imshow("Blue image", b)
cv2.waitKey(0)

# Convert BGR into HSV and display
hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
cv2.imshow("HSV image", hsv)
cv2.waitKey(0)
# Extract hue and display
h,s,v = cv2.split(hsv)
cv2.imshow("Hue image", h)
cv2.waitKey(0)
cv2.imshow("Saturation image", s)
cv2.waitKey(0)
cv2.imshow("Value image", v)
cv2.waitKey(0)

hsv_inrange = cv2.inRange(hsv, (125, 0, 0), (135, 255, 255))
cv2.imshow("Hue image", hsv_inrange)


kernelSizes = [(1,1),(3, 3), (5, 5),(7,7),(10,10), (40,40)]
# loop over the kernels sizes
for kernelSize in kernelSizes:
	# construct a rectangular kernel from the current size and then
	# apply an "opening" operation
	kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernelSize)
	erode = cv2.erode(v, kernel)
	cv2.imshow("Erode: ({}, {})".format(kernelSize[0], kernelSize[1]), erode)
	cv2.waitKey(0)
	ret, th = cv2.threshold(erode,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	cv2.imshow("Erode Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv2.waitKey(0)

for kernelSize in kernelSizes:
	# construct a rectangular kernel from the current size and then
	# apply an "opening" operation
	kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernelSize)
	dilate = cv2.dilate(v, kernel)
	cv2.imshow("Dilate: ({}, {})".format(
		kernelSize[0], kernelSize[1]), dilate)
	cv2.waitKey(0)
	ret, th = cv2.threshold(dilate,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	cv2.imshow("Dilate Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv2.waitKey(0)

for kernelSize in kernelSizes:
	# construct a rectangular kernel from the current size and then
	# apply an "opening" operation
	kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernelSize)
	opening = cv2.morphologyEx(v, cv2.MORPH_OPEN, kernel)
	cv2.imshow("Opening: ({}, {})".format(
		kernelSize[0], kernelSize[1]), opening)
	cv2.waitKey(0)
	ret, th = cv2.threshold(opening,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	cv2.imshow("Opening Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv2.waitKey(0)

for kernelSize in kernelSizes:
	# construct a rectangular kernel form the current size, but this
	# time apply a "closing" operation
	kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernelSize)
	closing = cv2.morphologyEx(v , cv2.MORPH_CLOSE, kernel)
	cv2.imshow("Closing: ({}, {})".format(
		kernelSize[0], kernelSize[1]), closing)
	cv2.waitKey(0)
	ret, th = cv2.threshold(closing,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	cv2.imshow("Closing Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv2.waitKey(0)
    
    
  
cv2.waitKey(0)
cv2.destroyAllWindows()

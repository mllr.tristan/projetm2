import sys
import cv2 as cv
import numpy as np

nameImg = 'pic6.png'

def drawCircles(img):
	rows = img.shape[0]
	res = cv.imread(nameImg, cv.IMREAD_COLOR)
	circles = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, rows / 8, 
								param1=30, param2=15 , 
								minRadius=0, maxRadius=40)
	if circles is not None:
		circles = np.uint16(np.around(circles))
		for i in circles[0, :]:
			center = (i[0], i[1])
			# circle center
			#cv.circle(res, center, 1, (0, 100, 100), 3)
			# circle outline
			radius = i[2]
			cv.circle(res, center, radius, (255, 0, 255), 3)
	return res

src = cv.imread(nameImg, cv.IMREAD_COLOR)
gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)

img = gray

kernelSizes = [(1,1),(3, 3), (5, 5),(10,10), (40,40)]

# loop over the kernels sizes
for kernelSize in kernelSizes:
	# construct a rectangular kernel from the current size and then
	# apply an "opening" operation
	kernel = cv.getStructuringElement(cv.MORPH_RECT, kernelSize)
	erode = cv.erode(img, kernel)
	#cv.imshow("Erode: ({}, {})".format(kernelSize[0], kernelSize[1]), erode)
	cv.imshow("Erode Circle: ({}, {})".format(kernelSize[0], kernelSize[1]), drawCircles(erode))
	cv.waitKey(0)
	ret, th = cv.threshold(erode,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
	#cv.imshow("Erode Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv.imshow("Erode Threshold Circle: ({}, {})".format(kernelSize[0], kernelSize[1]), drawCircles(th))
	cv.waitKey(0)

for kernelSize in kernelSizes:
	# construct a rectangular kernel from the current size and then
	# apply an "opening" operation
	kernel = cv.getStructuringElement(cv.MORPH_RECT, kernelSize)
	dilate = cv.dilate(img, kernel)
	#cv.imshow("Dilate: ({}, {})".format(kernelSize[0], kernelSize[1]), dilate)
	cv.imshow("Dilate Circle: ({}, {})".format(kernelSize[0], kernelSize[1]), drawCircles(dilate))
	cv.waitKey(0)
	ret, th = cv.threshold(dilate,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
	#cv.imshow("Dilate Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv.imshow("Dilate Threshold Circle: ({}, {})".format(kernelSize[0], kernelSize[1]), drawCircles(th))
	cv.waitKey(0)

for kernelSize in kernelSizes:
	# construct a rectangular kernel from the current size and then
	# apply an "opening" operation
	kernel = cv.getStructuringElement(cv.MORPH_RECT, kernelSize)
	opening = cv.morphologyEx(img, cv.MORPH_OPEN, kernel)
	#cv.imshow("Opening: ({}, {})".format(kernelSize[0], kernelSize[1]), opening)
	cv.imshow("Opening Circle: ({}, {})".format(kernelSize[0], kernelSize[1]), drawCircles(opening))
	cv.waitKey(0)
	ret, th = cv.threshold(opening,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
	#cv.imshow("Opening Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv.imshow("Opening Threshold Circle: ({}, {})".format(kernelSize[0], kernelSize[1]), drawCircles(th))
	cv.waitKey(0)

for kernelSize in kernelSizes:
	# construct a rectangular kernel form the current size, but this
	# time apply a "closing" operation
	kernel = cv.getStructuringElement(cv.MORPH_RECT, kernelSize)
	closing = cv.morphologyEx(img , cv.MORPH_CLOSE, kernel)
	#cv.imshow("Closing: ({}, {})".format(kernelSize[0], kernelSize[1]), closing)
	cv.imshow("Closing Circle: ({}, {})".format(kernelSize[0], kernelSize[1]), drawCircles(closing))
	cv.waitKey(0)
	ret, th = cv.threshold(closing,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
	#cv.imshow("Closing Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv.imshow("Closing Threshold Circle: ({}, {})".format(kernelSize[0], kernelSize[1]), drawCircles(th))
	cv.waitKey(0)
  
cv.waitKey(0)
cv.destroyAllWindows()


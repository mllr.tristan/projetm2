import sys
import cv2 as cv
import numpy as np

nameImg = 'pic6.png'

def drawCircles(img,p1,p2):
	rows = img.shape[0]
	res = cv.imread(nameImg, cv.IMREAD_COLOR)
	circles = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, rows / 8, 
								param1=p1, param2=p2 , 
								minRadius=0, maxRadius=40)
	if circles is not None:
		circles = np.uint16(np.around(circles))
		for i in circles[0, :]:
			center = (i[0], i[1])
			# circle center
			#cv.circle(res, center, 1, (0, 100, 100), 3)
			# circle outline
			radius = i[2]
			cv.circle(res, center, radius, (255, 0, 255), 3)
	return res

src = cv.imread(nameImg, cv.IMREAD_COLOR)
gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)

img = gray

kernelSize = (3, 3)

p1 = 23
p2 = 14
# construct a rectangular kernel form the current size, but this
# time apply a "closing" operation
kernel = cv.getStructuringElement(cv.MORPH_RECT, kernelSize)
closing = cv.morphologyEx(img , cv.MORPH_CLOSE, kernel)
ret, th = cv.threshold(closing,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
#cv.imshow("Closing Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
cv.imshow(f"Closing Threshold Circle: ({kernelSize[0]}, {kernelSize[0]}) / p1 = {p1} / p2 = {p2}",drawCircles(th,p1,p2))
cv.waitKey(0)
cv.destroyAllWindows()


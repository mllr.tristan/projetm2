# -*- coding: utf-8 -*-
"""
Created on Mon Sep 20 15:15:35 2021

@author: tmuller
"""

import cv2

import numpy as np

"""
Reading a single image
"""
# Read and display BGR image
bgr = cv2.imread ( 'pic4.jpg', cv2.IMREAD_UNCHANGED )

if (not bgr.data):
    print ('Impossible to read image !')

cv2.imshow("RGB image", bgr)
cv2.waitKey(0)
# Convert BGR into HSV and display
hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
cv2.imshow("HSV image", hsv)
cv2.waitKey(0)
# Extract hue and display
h,s,v = cv2.split(hsv)
#cv2.imshow("Hue image", h)
#cv2.waitKey(0)

gray = cv2.cvtColor(bgr, cv2.COLOR_BGR2GRAY)
#cv2.imshow("Gray image", gray)
#cv2.waitKey(0)

hsv_inrange = cv2.inRange(hsv, (125, 0, 0), (135, 255, 255))  
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7,7))
opening = cv2.morphologyEx(hsv_inrange, cv2.MORPH_OPEN, kernel)
cv2.imshow("Opening in range: (7,7)", opening)
cv2.imwrite("z_opening.png",opening)
cv2.waitKey(0)

kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (50,50))
erode = cv2.erode(s, kernel)
#cv2.imshow("Erode: (50,50)", erode)
#cv2.waitKey(0)
ret, th_erode = cv2.threshold(erode,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
cv2.imshow("Erode Threshold: (50,50)", th_erode)
cv2.imwrite("z_erode.png",th_erode)
cv2.waitKey(0)


kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7,7))
dilate = cv2.dilate(h, kernel)
#cv2.imshow("Dilate: (7,7)", dilate)
#cv2.waitKey(0)
ret, th_dilate = cv2.threshold(dilate,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
th_dilate = cv2.bitwise_not(th_dilate)
cv2.imshow("Dilate Threshold: (7,7)", th_dilate)
cv2.imwrite("z_dilate.png",th_dilate)
cv2.waitKey(0)

countour = th_dilate+th_erode+opening
cv2.imshow("Countours", countour)
cv2.imwrite("stickers.png", countour)
  
cv2.waitKey(0)
cv2.destroyAllWindows()

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QCheckBox, QComboBox, QFileDialog, QGridLayout, QGroupBox, QLabel,QWidget
from PyQt5.QtGui import QImage
import cv2, imutils
import numpy as np

#Program based on https://pyshine.com/Make-GUI-for-OpenCv-And-PyQt5/

class Window(QWidget):
	#Initilization Function
	def __init__(self, parent=None):
		super(Window, self).__init__(parent)
		self.setWindowTitle("BiAcoustic")
		self.resize(400, 300)
		
		self.filename = None # Will hold the image address location
		self.tmp = None # Will hold the temporary image for display
		self.rstValue()

		self.grid = QGridLayout()

		self.paramBox = QGroupBox("Parameters")
		self.paramGrid = QGridLayout()

		self.label = QtWidgets.QLabel()
		self.label.setText("")
		#self.label.setPixmap(QtGui.QPixmap("pic7.bmp"))
		self.label.setObjectName("label")
		
		self.saveButton = QtWidgets.QPushButton("Save Image")
		self.openButton = QtWidgets.QPushButton("Open Image")

		self.rstButton = QtWidgets.QPushButton("Reset Parameters")

		self.brightSlider = QtWidgets.QSlider()
		self.brightSlider.setOrientation(QtCore.Qt.Horizontal)
		self.brightLabel = QtWidgets.QLabel("Brightness")
		self.brightValSpinbox = QtWidgets.QSpinBox()
		self.brightValSpinbox.setKeyboardTracking(False)

		self.blurSlider = QtWidgets.QSlider()
		self.blurSlider.setOrientation(QtCore.Qt.Horizontal)
		self.blurLabel = QtWidgets.QLabel("Blurriness")
		self.blurValSpinbox = QtWidgets.QSpinBox()
		self.blurValSpinbox.setKeyboardTracking(False)

		self.minRadSlider = QtWidgets.QSlider()
		self.minRadSlider.setOrientation(QtCore.Qt.Horizontal)
		self.minRadSlider.setValue(self.minRad)
		self.minRadLabel = QtWidgets.QLabel("Minimum Radius")
		self.minRadValSpinbox = QtWidgets.QSpinBox()
		self.minRadValSpinbox.setKeyboardTracking(False)

		self.maxRadSlider = QtWidgets.QSlider()
		self.maxRadSlider.setOrientation(QtCore.Qt.Horizontal)
		self.maxRadSlider.setValue(self.maxRad)
		self.maxRadLabel = QtWidgets.QLabel("Maximum Radius")
		self.maxRadValSpinbox = QtWidgets.QSpinBox()
		self.maxRadValSpinbox.setKeyboardTracking(False)

		self.minDistSlider = QtWidgets.QSlider()
		self.minDistSlider.setOrientation(QtCore.Qt.Horizontal)
		self.minDistSlider.setValue(self.minDist)
		self.minDistLabel = QtWidgets.QLabel("Distance between centers")
		self.minDistValSpinbox = QtWidgets.QSpinBox()
		self.minDistValSpinbox.setKeyboardTracking(False)
		

		self.p1Slider = QtWidgets.QSlider()
		self.p1Slider.setOrientation(QtCore.Qt.Horizontal)
		self.p1Slider.setValue(self.p1)
		self.p1Label = QtWidgets.QLabel("Center Param")
		self.p1ValSpinbox = QtWidgets.QSpinBox()
		self.p1ValSpinbox.setKeyboardTracking(False)

		self.p2Slider = QtWidgets.QSlider()
		self.p2Slider.setOrientation(QtCore.Qt.Horizontal)
		self.p2Slider.setValue(self.p2)
		self.p2Label = QtWidgets.QLabel("Canny Param")
		self.p2ValSpinbox = QtWidgets.QSpinBox()
		self.p2ValSpinbox.setKeyboardTracking(False)

		self.closeSlider = QtWidgets.QSlider()
		self.closeSlider.setOrientation(QtCore.Qt.Horizontal)
		self.closeSlider.setValue(self.close_value_now)
		self.closeLabel = QtWidgets.QLabel("Close Kernel Size")
		self.closeValSpinbox = QtWidgets.QSpinBox()
		self.closeValSpinbox.setKeyboardTracking(False)


		self.paramGrid.addWidget(self.openButton, 0, 0,1,2)	
		self.paramGrid.addWidget(self.saveButton, 0, 2,1,2)

		self.filterBox = QGroupBox("Image Processing")
		self.filterGrid = QGridLayout()

		self.filterGrid.addWidget(self.closeLabel,1,0)
		self.filterGrid.addWidget(self.closeValSpinbox,1,1)
		self.filterGrid.addWidget(self.closeSlider, 1, 2, 1, 2)

		self.filterGrid.addWidget(self.minRadLabel,2,0)
		self.filterGrid.addWidget(self.minRadValSpinbox,2,1)
		self.filterGrid.addWidget(self.minRadSlider, 2, 2, 1, 2)

		self.filterGrid.addWidget(self.maxRadLabel,3,0)
		self.filterGrid.addWidget(self.maxRadValSpinbox,3,1)
		self.filterGrid.addWidget(self.maxRadSlider, 3, 2, 1, 2)

		self.filterGrid.addWidget(self.minDistLabel,4,0)
		self.filterGrid.addWidget(self.minDistValSpinbox,4,1)
		self.filterGrid.addWidget(self.minDistSlider, 4, 2, 1, 2)

		self.filterGrid.addWidget(self.p1Label,5,0)
		self.filterGrid.addWidget(self.p1ValSpinbox,5,1)
		self.filterGrid.addWidget(self.p1Slider, 5, 2, 1, 2)

		self.filterGrid.addWidget(self.p2Label,6,0)
		self.filterGrid.addWidget(self.p2ValSpinbox,6,1)
		self.filterGrid.addWidget(self.p2Slider, 6, 2, 1, 2)
		
		self.filterGrid.addWidget(self.brightLabel,7,0)
		self.filterGrid.addWidget(self.brightValSpinbox,7,1)
		self.filterGrid.addWidget(self.brightSlider, 7, 2, 1, 2)

		self.filterGrid.addWidget(self.blurLabel,8,0)
		self.filterGrid.addWidget(self.blurValSpinbox,8,1)
		self.filterGrid.addWidget(self.blurSlider, 8, 2, 1, 2)

		self.filterGrid.addWidget(self.rstButton,9,0,1,4)

		self.filterBox.setLayout(self.filterGrid)


		self.cursorsBox = QGroupBox("Cursors Parameters")
		self.cursorsGrid = QGridLayout()

		self.xMarkers = 300
		self.yMarker1 = 200
		self.yMarker2 = 400

		self.xMarkersSlider = QtWidgets.QSlider()
		self.xMarkersSlider.setRange(0,2000)
		self.xMarkersSlider.setOrientation(QtCore.Qt.Horizontal)
		self.xMarkersSlider.setValue(self.xMarkers)
		self.xMarkersLabel = QtWidgets.QLabel("Horizontal Position")
		self.xMarkersValSpinbox = QtWidgets.QSpinBox()

		self.yMarker1Slider = QtWidgets.QSlider()
		self.yMarker1Slider.setRange(0,2000)
		self.yMarker1Slider.setOrientation(QtCore.Qt.Horizontal)
		self.yMarker1Slider.setValue(self.yMarker1)
		self.yMarker1Label = QtWidgets.QLabel("Vertical Position Green")
		self.yMarker1ValSpinbox = QtWidgets.QSpinBox()

		self.yMarker2Slider = QtWidgets.QSlider()
		self.yMarker2Slider.setRange(0,2000)
		self.yMarker2Slider.setOrientation(QtCore.Qt.Horizontal)
		self.yMarker2Slider.setValue(self.yMarker2)
		self.yMarker2Label = QtWidgets.QLabel("Vertical Position Red")
		self.yMarker2ValSpinbox = QtWidgets.QSpinBox()
		
		self.sizeChamberLabel = QtWidgets.QLabel("Chamber Size")
		self.sizeChamberBox = QtWidgets.QSpinBox(self)
		self.sizeChamberBox.setKeyboardTracking(False)
		self.sizeChamberBox.setRange(0, 10000)
		self.sizeChamberBox.setSingleStep(100)
		self.sizeChamberBox.setSuffix(' um')

		self.cursorsGrid.addWidget(self.xMarkersLabel,0,0)
		self.cursorsGrid.addWidget(self.xMarkersValSpinbox,0,1)
		self.cursorsGrid.addWidget(self.xMarkersSlider, 0, 2, 1, 2)

		self.cursorsGrid.addWidget(self.yMarker1Label,1,0)
		self.cursorsGrid.addWidget(self.yMarker1ValSpinbox,1,1)
		self.cursorsGrid.addWidget(self.yMarker1Slider, 1, 2, 1, 2)

		self.cursorsGrid.addWidget(self.yMarker2Label,2,0)
		self.cursorsGrid.addWidget(self.yMarker2ValSpinbox,2,1)
		self.cursorsGrid.addWidget(self.yMarker2Slider, 2, 2, 1, 2)

		self.cursorsGrid.addWidget(self.sizeChamberLabel, 3, 0, 1, 2)
		self.cursorsGrid.addWidget(self.sizeChamberBox, 3, 2)
		
		self.cursorsBox.setLayout(self.cursorsGrid)

		self.measureBox = QGroupBox("Measurement")
		self.measureGrid = QGridLayout()

		self.drawMeasureCheck = QCheckBox()
		self.drawMeasureLabel = QLabel("Draw measurements on the image")

		self.meanRadiusLabel = QLabel("Mean radius of Bubbles: ")
		self.meanRadius = QLabel("TBD")
		self.meanRadiusUnit = QLabel("um")

		self.medianRadiusLabel = QLabel("Median radius of Bubbles: ")
		self.medianRadius = QLabel("TBD")
		self.medianRadiusUnit = QLabel("um")

		self.firstPercentileLabel = QLabel("1st Percentile")
		self.firstPercentile = QLabel("TBD")
		self.firstPercentileUnit = QLabel("um")

		self.thirdPercentileLabel = QLabel("3rd Percentile")
		self.thirdPercentile = QLabel("TBD")
		self.thirdPercentileUnit = QLabel("um")

		self.maxRadiusLabel = QLabel("Maximum Radius")
		self.maxRadius = QLabel("TBD")
		self.maxRadiusUnit = QLabel("um")

		self.minRadiusLabel = QLabel("Minimum Radius")
		self.minRadius = QLabel("TBD")
		self.minRadiusUnit= QLabel("um")

		self.measureGrid.addWidget(self.meanRadiusLabel,0,0)
		self.measureGrid.addWidget(self.meanRadius, 0, 1)
		self.measureGrid.addWidget(self.meanRadiusUnit, 0,2)

		self.measureGrid.addWidget(self.medianRadiusLabel,1,0)
		self.measureGrid.addWidget(self.medianRadius,1,1)
		self.measureGrid.addWidget(self.medianRadiusUnit,1,2)

		self.measureGrid.addWidget(self.firstPercentileLabel,2,0)
		self.measureGrid.addWidget(self.firstPercentile,2,1)
		self.measureGrid.addWidget(self.firstPercentileUnit,2,2)

		self.measureGrid.addWidget(self.minRadiusLabel,3,0)
		self.measureGrid.addWidget(self.minRadius,3,1)
		self.measureGrid.addWidget(self.minRadiusUnit,3,2)

		self.measureGrid.addWidget(self.maxRadiusLabel,4,0)
		self.measureGrid.addWidget(self.maxRadius,4,1)
		self.measureGrid.addWidget(self.maxRadiusUnit,4,2)

		self.measureBox.setLayout(self.measureGrid)


		self.paramGrid.addWidget(self.filterBox,2,0,1,4)
		self.paramGrid.addWidget(self.cursorsBox,3,0,1,4)
		self.paramGrid.addWidget(self.measureBox,4,0,1,4)

		self.paramBox.setLayout(self.paramGrid)
		self.paramBox.setMinimumWidth(500)
		
		self.grid.addWidget(self.paramBox, 0,0)

		imgBox = QGroupBox("Image")
		imgGrid = QGridLayout()

		spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
		imgGrid.addWidget(self.label,1,1,1,1)
		imgBox.setLayout(imgGrid)
		self.grid.addWidget(imgBox,0,1)

		self.setLayout(self.grid)

		self.image = cv2.imread("pic6.png")
		self.setPhoto(self.image)
		self.rstParam()
		self.update()

		self.minRadSlider.valueChanged['int'].connect(self.minRad_value)
		self.maxRadSlider.valueChanged['int'].connect(self.maxRad_value)
		self.minDistSlider.valueChanged['int'].connect(self.minDist_value)
		self.brightSlider.valueChanged['int'].connect(self.brightness_value)
		self.blurSlider.valueChanged['int'].connect(self.blur_value)
		self.p1Slider.valueChanged['int'].connect(self.p1_value)
		self.p2Slider.valueChanged['int'].connect(self.p2_value)
		self.closeSlider.valueChanged['int'].connect(self.close_value)

		self.minRadValSpinbox.valueChanged.connect(self.minRad_value)
		self.maxRadValSpinbox.valueChanged.connect(self.maxRad_value)
		self.minDistValSpinbox.valueChanged.connect(self.minDist_value)
		self.brightValSpinbox.valueChanged.connect(self.brightness_value)
		self.blurValSpinbox.valueChanged.connect(self.blur_value)
		self.p1ValSpinbox.valueChanged.connect(self.p1_value)
		self.p2ValSpinbox.valueChanged.connect(self.p2_value)
		self.closeValSpinbox.valueChanged.connect(self.close_value)

		self.openButton.clicked.connect(self.loadImage)
		self.saveButton.clicked.connect(self.savePhoto)
		self.rstButton.clicked.connect(self.rstParam)

		self.xMarkersSlider.valueChanged['int'].connect(self.xMarkers_value)
		self.yMarker1Slider.valueChanged['int'].connect(self.yMarker1_value)
		self.yMarker2Slider.valueChanged['int'].connect(self.yMarker2_value)
		self.sizeChamberBox.valueChanged.connect(self.sizeChamber_value)

	
	def loadImage(self):
		""" This function will load the user selected image
			and set it to label using the setPhoto function
		"""
		self.filename = QFileDialog.getOpenFileName(filter="Image (*.*)")[0]
		self.image = cv2.imread(self.filename)
		self.setPhoto(self.image)
		self.update()
		#print(len(self.image.shape))
	
	def setPhoto(self,image):
		""" This function will take image input and resize it 
			only for display purpose and convert it to QImage
			to set at the label.
		"""
		self.tmp = image
		image = imutils.resize(image,width=1024)
		frame = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
		image = QImage(frame, frame.shape[1],frame.shape[0],frame.strides[0],QImage.Format_RGB888)
		self.label.setPixmap(QtGui.QPixmap.fromImage(image))
	
	def brightness_value(self,value,up=True):
		""" This function will take value from the slider
			for the brightness from 0 to 99
		"""
		self.brightness_value_now = value
		print('Brightness: ',value)
		self.brightValSpinbox.blockSignals(True)
		self.brightValSpinbox.setValue(self.brightness_value_now)
		self.brightSlider.setValue(value)
		self.brightValSpinbox.blockSignals(False)
		if up:
			self.update()
		
		
	def blur_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.blur_value_now = value
		print('Blur: ',value)
		self.blurValSpinbox.blockSignals(True)
		self.blurValSpinbox.setValue(self.blur_value_now)
		self.blurSlider.setValue(value)
		self.blurValSpinbox.blockSignals(False)
		if up:
			self.update()

	def p1_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.p1 = value+1
		print('P1: ',self.p1)
		self.p1ValSpinbox.blockSignals(True)
		self.p1ValSpinbox.setValue(self.p1)
		self.p1Slider.setValue(value)
		self.p1ValSpinbox.blockSignals(False)
		if up:
			self.update()
	
	def p2_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.p2 = value+1
		print('P2: ',self.p2)
		self.p2ValSpinbox.blockSignals(True)
		self.p2ValSpinbox.setValue(self.p2)
		self.p2Slider.setValue(value)
		self.p2ValSpinbox.blockSignals(False)
		if up:
			self.update()

	def close_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.close_value_now = value+1
		print('Close Kernel Size: ',self.close_value_now)
		self.closeValSpinbox.blockSignals(True)
		self.closeValSpinbox.setValue(self.close_value_now)
		self.closeSlider.setValue(value)
		self.closeValSpinbox.blockSignals(False)
		if up:
			self.update()

	def minRad_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.minRad = value
		print('Minimum Radius: ',self.minRad)
		self.minRadValSpinbox.blockSignals(True)
		self.minRadValSpinbox.setValue(self.minRad)
		self.minRadSlider.setValue(value)
		self.minRadValSpinbox.blockSignals(False)
		if up:
			self.update()
	
	def maxRad_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.maxRad = value
		print('Maximum radius: ',self.maxRad)
		self.maxRadValSpinbox.blockSignals(True)
		self.maxRadValSpinbox.setValue(self.maxRad)
		self.maxRadSlider.setValue(value)
		self.maxRadValSpinbox.blockSignals(False)
		if up:
			self.update()
	
	def minDist_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.minDist = value+1
		print('Minimum distance between centers: ',self.minDist)
		self.minDistValSpinbox.blockSignals(True)
		self.minDistValSpinbox.setValue(self.minDist)
		self.minDistSlider.setValue(value)
		self.minDistValSpinbox.blockSignals(False)
		if up:
			self.update()
	
	def xMarkers_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.xMarkers = value
		print('X coord Marker 1: ',self.xMarkers)
		self.xMarkersValSpinbox.blockSignals(True)
		self.xMarkersValSpinbox.setValue(self.xMarkers)
		self.xMarkersSlider.setValue(value)
		self.xMarkersValSpinbox.blockSignals(False)
		if up:
			self.update()

	def yMarker1_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.yMarker1 = value
		print('Y coord Marker 1: ',self.yMarker1)
		self.yMarker1ValSpinbox.blockSignals(True)
		self.yMarker1ValSpinbox.setValue(self.yMarker1)
		self.yMarker1Slider.setValue(value)
		self.yMarker1ValSpinbox.blockSignals(False)
		if up:
			self.update()

	def yMarker2_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.yMarker2 = value
		print('Y coord Marker 2: ',self.yMarker2)
		self.yMarker2ValSpinbox.blockSignals(True)
		self.yMarker2ValSpinbox.setValue(self.yMarker2)
		self.yMarker2Slider.setValue(value)
		self.yMarker2ValSpinbox.blockSignals(False)
		if up:
			self.update()
	
	def sizeChamber_value(self,value,up=True):
		""" This function will take value from the slider 
			for the blur from 0 to 99 """
		self.sizeChamber = value
		print(f'Size chamber: {self.sizeChamber} um')
		self.sizeChamberBox.setValue(value)
		if up:
			self.update()
	
	def rstParam(self):
		self.rstValue()
		self.minDist_value(self.minDist,False)
		self.maxRad_value(self.maxRad,False)
		self.minRad_value(self.minRad,False)
		self.close_value(self.close_value_now,False)
		self.blur_value(self.blur_value_now,False)
		self.brightness_value(self.brightness_value_now,False)
		self.xMarkers_value(self.xMarkers,False)
		self.yMarker1_value(self.yMarker1,False)
		self.yMarker2_value(self.yMarker2,False)	
		self.sizeChamber_value(self.sizeChamber,False)
		self.update()
	
	def rstValue(self):
		self.brightness_value_now = 0 # Updated brightness value
		self.blur_value_now = 0 # Updated blur value
		self.close_value_now = 3-1
		self.p1 = 23
		self.p2 = 14
		self.minRad = 0
		self.maxRad = 40
		self.minDist = 8
		self.sizeChamber = 1100

	
	def changeBrightness(self,img,value):
		""" This function will take an image (img) and the brightness
			value. It will perform the brightness change using OpenCv
			and after split, will merge the img and return it.
		"""
		hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
		h,s,v = cv2.split(hsv)
		lim = 255 - value
		v[v>lim] = 255
		v[v<=lim] += value
		final_hsv = cv2.merge((h,s,v))
		img = cv2.cvtColor(final_hsv,cv2.COLOR_HSV2BGR)
		return img
		
	def changeBlur(self,img,value):
		""" This function will take the img image and blur values as inputs.
			After perform blur operation using opencv function, it returns 
			the image img.
		"""
		kernel_size = (value+1,value+1) # +1 is to avoid 0
		img = cv2.blur(img,kernel_size)
		return img
	
	def update(self):
		""" This function will update the photo according to the 
			current values of blur and brightness and set it to photo label.
		"""
		img = self.changeBrightness(self.image,self.brightness_value_now)
		img = self.changeBlur(img,self.blur_value_now)
		bw = self.changeClose(img,self.close_value_now)
		img = self.drawCircles(img,bw,self.p1,self.p2)
		self.p1ValSpinbox.setValue(self.p1)
		self.p2ValSpinbox.setValue(self.p2)
		cv2.drawMarker(img, (int(self.xMarkers),int(self.yMarker1)), color=(0,255,0), markerType=cv2.MARKER_CROSS, thickness=2)
		cv2.drawMarker(img, (int(self.xMarkers),int(self.yMarker2)), color=(0,0,255), markerType=cv2.MARKER_CROSS, thickness=2)
		self.setPhoto(img)

		self.xMarkersSlider.setRange(0,img.shape[1])
		self.yMarker1Slider.setRange(0,img.shape[0])
		self.yMarker2Slider.setRange(0,img.shape[0])

		#Measurements:
		if self.yMarker1 != self.yMarker2: #Avoid divide by zero
			self.coeffSize = np.abs(self.sizeChamber / (self.yMarker2 - self.yMarker1))
		else:
			print("Move the cursors away from each others")
		#print(self.coeffSize)
		if self.circles is not None:
			radius = []
			self.circles = np.uint16(np.around(self.circles))
			for i in self.circles[0, :]:
				radius.append(i[2]*self.coeffSize)
			self.meanRadius.setText("{:.3f}".format(np.mean(radius)))
			self.medianRadius.setText("{:.3f}".format(np.median(radius)))
			self.firstPercentile.setText("{:.3f}".format(np.percentile(radius,25)))
			self.thirdPercentile.setText("{:.3f}".format(np.percentile(radius,75)))
			self.maxRadius.setText("{:.3f}".format(np.max(radius)))
			self.minRadius.setText("{:.3f}".format(np.min(radius)))
		else:
			self.meanRadius.setText("TBD")
			self.medianRadius.setText("TBD")
			self.firstPercentile.setText("TBD")
			self.thirdPercentile.setText("TBD")
			self.maxRadius.setText("TBD")
			self.minRadius.setText("TBD")

			# Moyenne, médiane, 1 et 3 eme quartile et val max min


	
	def savePhoto(self):
		""" This function will save the image"""
		# here provide the output file name
		# lets say we want to save the output as a time stamp
		# uncomment the two lines below
		
		# import time
		# filename = 'Snapshot '+str(time.strftime("%Y-%b-%d at %H.%M.%S %p"))+'.png'
		
		# Or we can give any name such as output.jpg or output.png as well
		# filename = 'Snapshot.png'	
	
		# Or a much better option is to let user decide the location and the extension
          	# using a file dialog.
		
		filename = QFileDialog.getSaveFileName(filter="JPG(*.jpg);;PNG(*.png);;TIFF(*.tiff);;BMP(*.bmp)")[0]
		
		cv2.imwrite(filename,self.tmp)
		print('Image saved as:',self.filename)

	def drawCircles(self,src,img,p1,p2):
		#print(f"{p1},{p2}")
		res = src
		rows = img.shape[0]
		#res = cv2.imread(nameImg, cv2.IMREAD_COLOR)
		self.circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, rows / self.minDist, 
									param1=p1, param2=p2 , 
									minRadius=self.minRad, maxRadius=self.maxRad)
		if self.circles is not None:
			self.circles = np.uint16(np.around(self.circles))
			for i in self.circles[0, :]:
				center = (i[0], i[1])
				# circle center
				#cv.circle(res, center, 1, (0, 100, 100), 3)
				# circle outline
				radius = i[2]
				cv2.circle(res, center, radius, (255, 0, 255), 3)
		return res
	
	def changeClose(self,img,sizekernel):
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		kernelSize = (sizekernel,sizekernel)
		#print(kernelSize)
		kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernelSize)
		closing = cv2.morphologyEx(img , cv2.MORPH_CLOSE, kernel)
		ret, th = cv2.threshold(closing,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
		return th

	


if __name__ == "__main__":
	import sys
	app = QtWidgets.QApplication(sys.argv)
	MainWindow = Window()
	MainWindow.show()
	sys.exit(app.exec_())
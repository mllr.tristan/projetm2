import sys
import cv2 as cv
import numpy as np

src = cv.imread('pic4.jpg', cv.IMREAD_COLOR)
gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)

kernelSizes = [(1,1),(3, 3), (5, 5),(7,7),(10,10), (40,40)]
# loop over the kernels sizes
for kernelSize in kernelSizes:
	# construct a rectangular kernel from the current size and then
	# apply an "opening" operation
	kernel = cv.getStructuringElement(cv.MORPH_RECT, kernelSize)
	erode = cv.erode(gray, kernel)
	cv.imshow("Erode: ({}, {})".format(kernelSize[0], kernelSize[1]), erode)
	cv.waitKey(0)
	ret, th = cv.threshold(erode,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
	cv.imshow("Erode Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv.waitKey(0)

for kernelSize in kernelSizes:
	# construct a rectangular kernel from the current size and then
	# apply an "opening" operation
	kernel = cv.getStructuringElement(cv.MORPH_RECT, kernelSize)
	dilate = cv.dilate(gray, kernel)
	cv.imshow("Dilate: ({}, {})".format(
		kernelSize[0], kernelSize[1]), dilate)
	cv.waitKey(0)
	ret, th = cv.threshold(dilate,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
	cv.imshow("Dilate Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv.waitKey(0)

for kernelSize in kernelSizes:
	# construct a rectangular kernel from the current size and then
	# apply an "opening" operation
	kernel = cv.getStructuringElement(cv.MORPH_RECT, kernelSize)
	opening = cv.morphologyEx(gray, cv.MORPH_OPEN, kernel)
	cv.imshow("Opening: ({}, {})".format(
		kernelSize[0], kernelSize[1]), opening)
	cv.waitKey(0)
	ret, th = cv.threshold(opening,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
	cv.imshow("Opening Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv.waitKey(0)

for kernelSize in kernelSizes:
	# construct a rectangular kernel form the current size, but this
	# time apply a "closing" operation
	kernel = cv.getStructuringElement(cv.MORPH_RECT, kernelSize)
	closing = cv.morphologyEx(gray , cv.MORPH_CLOSE, kernel)
	cv.imshow("Closing: ({}, {})".format(
		kernelSize[0], kernelSize[1]), closing)
	cv.waitKey(0)
	ret, th = cv.threshold(closing,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
	cv.imshow("Closing Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
	cv.waitKey(0)
    
    
  
cv.waitKey(0)
cv.destroyAllWindows()
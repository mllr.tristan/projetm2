import sys
import cv2 as cv
import numpy as np
def main(argv):
    
    # Loads an image
    src = cv.imread('pic4.jpg', cv.IMREAD_COLOR)
    
    
    gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)
    #hsv = cv.cvtColor(src, cv.COLOR_BGR2HSV)
    #h,s,v = cv.split(hsv)
    kernelSize = (1,1)
    
    kernel = cv.getStructuringElement(cv.MORPH_ERODE, kernelSize)
    erode = cv.erode(v, kernel)
    ret, th = cv.threshold(erode,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
    cv.imshow("Erode Threshold: ({}, {})".format(kernelSize[0], kernelSize[1]), th)
    
    th = erode
      
    rows = th.shape[0]
    circles = cv.HoughCircles(th, cv.HOUGH_GRADIENT, 1, rows / 4,
                               param1=300, param2=30,
                               minRadius=0, maxRadius=90)
    
    
    if circles is not None:
        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            center = (i[0], i[1])
            # circle center
            cv.circle(th, center, 1, (0, 100, 100), 3)
            # circle outline
            radius = i[2]
            cv.circle(th, center, radius, (255, 0, 255), 3)
    
    
    cv.imshow("detected circles", th)
    cv.waitKey(0)
    
    return 0
if __name__ == "__main__":
    main(sys.argv[1:])
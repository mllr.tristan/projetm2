import sys
import cv2 as cv
import numpy as np

src = cv.imread('pic4.jpg', cv.IMREAD_COLOR)
hsv = cv.cvtColor(src, cv.COLOR_BGR2HSV)
gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)
hls = cv.cvtColor(src, cv.COLOR_BGR2HLS)
ycrcb = cv.cvtColor(src, cv.COLOR_BGR2YCR_CB)
xyz = cv.cvtColor(src, cv.COLOR_BGR2XYZ)

cv.imshow("Source",src)

cv.imshow("HSV",hsv)
h,s,v = cv.split(hsv)
cv.imshow("Hsv",h)
cv.imshow("hSv",s)
cv.imshow("hsV",v)

cv.imshow("Gray",gray)

cv.imshow("HLS",hls)
h2,l2,s2 = cv.split(hls)
cv.imshow("Hls",h2)
cv.imshow("hLs",l2)
cv.imshow("hlS",s2)

cv.imshow("YCrCb",ycrcb)
y,cr,cb = cv.split(ycrcb)
cv.imshow("Y",y)
cv.imshow("Cr",cr)
cv.imshow("Cb",cb)

cv.imshow("XYZ",xyz)
x,y,z = cv.split(xyz)
cv.imshow("Xyz",x)
cv.imshow("xYz",y)
cv.imshow("xyZ",z)
cv.waitKey(0)
import torch

# Model
model = torch.hub.load('yolov5','custom',path='best500s',source='local')  # or yolov5m, yolov5l, yolov5x, custom

# Images
img = '0.png'  # or file, Path, PIL, OpenCV, numpy, list

# Inference
results = model(img)

# Results
results.show()  # or .show(), .save(), .crop(), .pandas(), etc.

print(results.pandas().xyxy)

# xyxy indicates the format of the result. formats available are:
# xyxy (xmin,ymin,xmax,ymax)
# xywh (xmin,ymin,xmax,ymax)
# xywhn - normalized

import torch
import cv2

imgPath = '0.png'  # or file, Path, PIL, OpenCV, numpy, list
modelPath = 'best500s'

img = cv2.imread (imgPath)

# Model
model = torch.hub.load('yolov5','custom',path=modelPath,source='local')  # or yolov5m, yolov5l, yolov5x, custom

# Inference
results = model(imgPath)

# Results
#results.show()  # or .show(), .save(), .crop(), .pandas(), etc.

conf = 0.7

res = results.xyxy[0].tolist()
boxes = []

for box in res:
	if box[4] > conf:
		boxes.append([int(box[0]),int(box[1]),int(box[2]),int(box[3])])
		cv2.rectangle(img,(int(box[0]),int(box[1])),(int(box[2]),int(box[3])),(0,0,255),1)
		
#print(boxes)

cv2.imshow("Boxes",img)
cv2.waitKey(0)

# xyxy indicates the format of the result. formats available are:
# xyxy (xmin,ymin,xmax,ymax)
# xywh (xmin,ymin,xmax,ymax)
# xywhn - normalized

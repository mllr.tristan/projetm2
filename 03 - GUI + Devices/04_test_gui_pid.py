import sys,os,csv
from functools import partial #For argument through buttons
from PyQt5.QtCore import Qt,QTimer
from PyQt5.QtWidgets import (QApplication, QMessageBox, QCheckBox, QGridLayout, QGroupBox, QLineEdit,
        QMenu, QPushButton, QRadioButton, QVBoxLayout, QWidget,QComboBox,QLabel)
import serial.tools.list_ports
import numpy as np
import pyqtgraph as pg
import Py_P_Pump
import propar
import time
from simple_pid import PID

class Window(QWidget):
    #Initilization Function
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        # PID Parameters
        self.Kp = 1
        self.Ki = 1
        self.Kd = 1

        #Variable that need definitions befrore being used
        self.hasStop = False #Used for stopping and continuing the plot
        self.offsetTime = 0 #Used to store the offset time when stopping the plot
        self.flowCmdSent = 0 #Flowrate input by the user when the button change flowrate is pressed
        self.pidActive = False #Used to calculate the PID or not depending on the buttons pressed
        
        #Definition of the PID
        self.pid = PID(self.Kp,self.Ki,self.Kd,100) #100 is the first target
        self.pid.output_limits = (30, 2500) #in mPa
        
        #Group box for parameters
        comBox = QGroupBox("COM Ports")
        comGrid = QGridLayout()

        refreshButton = QPushButton('Refresh COM Ports', self)
        refreshButton.clicked.connect(self.refreshMethod)

        comGrid.addWidget(refreshButton,1,0,1,4)

        comPortsCPLabel = QLabel("Continuous Phase Pump", self) 
        self.comPortsCP = QComboBox()
        self.stateCPLabel = QLabel("Disconnected",self)

        comGrid.addWidget(comPortsCPLabel,2,0)
        comGrid.addWidget(self.comPortsCP,2,1)
        comGrid.addWidget(self.stateCPLabel,2,2,1,2)

        comPortsDPLabel = QLabel("Dispersed Phase Pump", self)
        self.comPortsDP = QComboBox()
        self.stateDPLabel = QLabel("Disconnected",self)

        comGrid.addWidget(comPortsDPLabel,3,0)
        comGrid.addWidget(self.comPortsDP,3,1)        
        comGrid.addWidget(self.stateDPLabel,3,2,1,2)

        comPortsFMLabel = QLabel("Flowmeter", self)
        self.stateFMLabel = QLabel("Disconnected", self)
        self.comPortsFM = QComboBox() #BronkHorst FlowMeter
        
        comGrid.addWidget(comPortsFMLabel,4,0)
        comGrid.addWidget(self.comPortsFM,4,1)        
        comGrid.addWidget(self.stateFMLabel,4,2,1,2)

        connectButton = QPushButton('Connect to instruments', self)
        connectButton.clicked.connect(self.connectMethod)  
        disconnectButton = QPushButton('Disconnect from instruments', self)
        disconnectButton.clicked.connect(self.disconnectMethod)
        
        comGrid.addWidget(connectButton,5,0,1,2)
        comGrid.addWidget(disconnectButton,5,2,1,2)

        self.testPump = QComboBox()
        self.testPressure = QComboBox()
        self.testPressure.addItems(['0','100','200','400','600','800','1000'])
        testPressureLabel = QLabel("mPa")
        testPressureButton = QPushButton("Test Pump", self)
        testPressureButton.clicked.connect(self.testPressureMethod)

        comGrid.addWidget(self.testPump,6,0)
        comGrid.addWidget(self.testPressure,6,1)
        comGrid.addWidget(testPressureLabel,6,2)
        comGrid.addWidget(testPressureButton,6,3)

        self.testFlow = QComboBox()
        self.testFlowResult = QLabel("No data")
        testFlowLabel = QLabel("uL/min")
        testFlowButton = QPushButton("Test Flow Measure", self)
        testFlowButton.clicked.connect(self.testFlowmeterMethod)

        comGrid.addWidget(self.testFlow,7,0)
        comGrid.addWidget(self.testFlowResult,7,1)
        comGrid.addWidget(testFlowLabel,7,2)
        comGrid.addWidget(testFlowButton,7,3)
       
        comBox.setMaximumWidth(500)
        comBox.setLayout(comGrid)

        #Parameters group bax

        paramBox = QGroupBox("Parameters")
        paramGrid = QGridLayout()

        self.csvCheckbox = QCheckBox("Save as CSV")
        self.csvFilenameLabel = QLabel("File name:")
        self.csvFilename = QLineEdit()

        t = time.localtime()
        current_time = time.strftime("%Y_%m_%d_%H_%M_%S", t)
        self.csvFilename.setText(current_time)
        self.csvCheckbox.setChecked(True)

        paramGrid.addWidget(self.csvFilenameLabel,0,0)
        paramGrid.addWidget(self.csvFilename,0,1,1,2)
        paramGrid.addWidget(self.csvCheckbox,0,3)

        self.freq = QComboBox()
        freqLabel = QLabel("Sampling Frequency (Approx.)")
        freqLabelHz = QLabel("Hz") 

        self.freq.addItems(['0.1','0.5','1','2','5','10','20'])
        self.freq.setCurrentIndex(3)

        paramGrid.addWidget(freqLabel,1,0)
        paramGrid.addWidget(self.freq,1,1)
        paramGrid.addWidget(freqLabelHz,1,2)

        self.measureTime = QLineEdit()
        measureTimeLabel = QLabel("Plot width")
        measureTimeLabelUnit = QLabel("s")
        self.timeCheckbox = QCheckBox("Continuous")
        self.timeCheckbox.toggle()

        paramGrid.addWidget(measureTimeLabel,2,0)
        paramGrid.addWidget(self.measureTime,2,1)
        paramGrid.addWidget(measureTimeLabelUnit,2,2)
        paramGrid.addWidget(self.timeCheckbox,2,3)

        
        runButton = QPushButton("Run Measurement", self)
        runButton.clicked.connect(self.startMethod)
        stopButton = QPushButton("Stop measurement",self)
        stopButton.clicked.connect(self.stopMethod)
        clearButton = QPushButton("ClearPlot",self)
        clearButton.clicked.connect(self.clearMethod)
       
        paramGrid.addWidget(runButton,3,0,1,1)
        paramGrid.addWidget(stopButton,3,1,1,2)
        paramGrid.addWidget(clearButton,3,3)

        paramBox.setMaximumWidth(500)
        paramBox.setLayout(paramGrid)

        #Group Box for pressure control
        pressBox = QGroupBox("Pressure Control")
        pressGrid = QGridLayout()

        self.pressureContinuous = QLineEdit()
        self.pressureDispersed = QLineEdit()
        presCLabel = QLabel('Pressure Continuous Phase (mPa)')
        presDLabel = QLabel('Pressure Dispersed Phase (mPa)')

        pressGrid.addWidget(presCLabel,0,0,1,2)
        pressGrid.addWidget(self.pressureContinuous,0,3)
        pressGrid.addWidget(presDLabel,1,0,1,2)
        pressGrid.addWidget(self.pressureDispersed,1,3)

        pressButton = QPushButton("Change pressure", self)
        pressButton.clicked.connect(self.setPressMethod)
        self.setPressC = 0
        self.setPressD = 0

        pressGrid.addWidget(pressButton,2,0,1,4)

        pressBox.setMaximumWidth(500)
        pressBox.setLayout(pressGrid)

        #Group Box for Flow control
        flowBox = QGroupBox("Flow Control")
        flowGrid = QGridLayout()
        
        self.flowCmd = QLineEdit()
        presDFlowLabel = QLabel('Pressure Dispersed Phase (mPa)')
        self.pressureDispersedFlow = QLineEdit()
        flowCmdLabel = QLabel("Continuous Flowrate Setpoint (uL/min)")
        flowButton = QPushButton("Change flowrate")

        flowButton.clicked.connect(self.setFlowMethod)

        
        flowGrid.addWidget(flowCmdLabel,0,0,1,2)
        flowGrid.addWidget(self.flowCmd,0,3)
        flowGrid.addWidget(presDFlowLabel,1,0,1,2)
        flowGrid.addWidget(self.pressureDispersedFlow,1,3)
        flowGrid.addWidget(flowButton,2,0,1,4)

        flowBox.setMaximumWidth(500)
        flowBox.setLayout(flowGrid)

        #Group box for plots
        plotBox = QGroupBox("Plots")
        plotGrid = QGridLayout()

        self.graphPressure = pg.PlotWidget()
        self.graphFlow = pg.PlotWidget()
        self.graphPressure.setMinimumWidth(700)
        self.graphFlow.setMinimumWidth(700)
        self.clearMethod() #Initialize some variables for the plot

        self.graphPressure.setLabel('bottom', 'Time (s)')
        self.graphFlow.setLabel('bottom', 'Time (s)')
        self.graphPressure.setLabel('left', 'Pressure (mPa)')
        self.graphFlow.setLabel('left', 'Flow (uL/min)')

        #Definition of pen to trace on the plots
        self.pen1 = pg.mkPen(color=(255, 0, 0))
        self.pen2 = pg.mkPen(color=(0, 255, 0))

        self.graphFlow.showGrid(x = True, y = True, alpha = 0.3)  
        self.graphPressure.showGrid(x = True, y = True, alpha = 0.3)  

        plotGrid.addWidget(self.graphPressure,0,0)
        plotGrid.addWidget(self.graphFlow,1,0)
        plotBox.setLayout(plotGrid)

        #Grid 
        grid = QGridLayout()
        grid.addWidget(comBox, 0, 0)
        grid.addWidget(paramBox, 1, 0)
        grid.addWidget(pressBox,2,0)
        grid.addWidget(flowBox,3,0)
        grid.addWidget(plotBox, 0, 1,4,1)
        self.setLayout(grid)
      
        self.setWindowTitle("BiAcoustic")
        self.resize(400, 300)

    #Function to simply the errors raised by the COM Ports
    def errorPort(self,port):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)

        msg.setText(f"Can't communicate with the port {port}")
        msg.setInformativeText("Check if it's correct or if it's used by another software")
        msg.exec_()

    #Function to clear the 2 graphs in the window
    def clearMethod(self):
        #Reset start time
        self.startTime = time.time()
        #Empty the lists containing the measurements
        self.time = []
        self.plotPressC = []
        self.plotPressD = []
        self.plotFlow = []
        self.plotFlowCmd = []
        #Clear the graphs
        self.graphFlow.clear()
        self.graphPressure.clear()
    
    #Function to start the measurement
    def startMethod(self):
        if self.time == []:
            #If there is no data yet, store a new start time
            self.startTime = time.time()
        else:
            #Else use the previously stored one
            if self.endTime != None:
                self.startTime = self.endTime #Get the last time used
            if self.hasStop == True:
                self.offsetTime = time.time()-self.endTime-self.time[-1]
                self.hasStop = False
        
        #Create a timer to update the graph at the frequency of your choice
        self.timer = QTimer()
        #Look for errors while definning its interval and its function
        try:
            self.timer.setInterval(int(1000/int(self.freq.currentText())))
            self.timer.timeout.connect(self.update_plot_data)
            self.timer.start()
        except Exception as e: 
            print(f"Error: {e}")
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)

            msg.setText(f"Incorrect frequency")
            msg.setInformativeText("Please use only numbers")
            msg.exec_()

        #Check if the user want a backup as CSV
        if self.csvCheckbox.isChecked():
            #Check if the file already exist
            if os.path.isfile(f"{self.csvFilename.text()}.csv"):
                print("File already exist")
            else:
                print("New file")
                try:
                    with open(f"{self.csvFilename.text()}.csv", 'w', newline='') as csvfile:
                        f = csv.writer(csvfile, delimiter=',')
                        f.writerow(['Time','Flow Measured (uL/min)',
                                    'Flow setpoint(uL/min)','Continuous Phase Pressure (mPa)', 
                                    'Dispersed Phase Pressure (mPa'])
                except Exception as e: 
                    print(f"Error: {e}")
                    msg = QMessageBox()
                    msg.setIcon(QMessageBox.Critical)

                    msg.setText(f"Error while writing in the CSV")
                    msg.setInformativeText("Please check the CSV file name")
                    msg.exec_()

    #Function that update the plot data and the plot image
    def update_plot_data(self):
        self.endTime = time.time()
        if self.endTime>self.startTime:
            if not(self.timeCheckbox.isChecked()):
                try:
                    if len(self.time)>(int(self.freq.currentText())*int(self.measureTime.text())):
                        self.plotPressC = self.plotPressC[1:]  # Remove the first
                        self.plotPressD = self.plotPressD[1:]  # Remove the first
                        self.plotFlow = self.plotFlow[1:]      # Remove the first
                        self.time = self.time[1:]      # Remove the first
                except Exception as e: 
                    print(f"Error: {e}")
                    msg = QMessageBox()
                    msg.setIcon(QMessageBox.Critical)

                    msg.setText(f"Incorrect window")
                    msg.setInformativeText("Please check frequency and window width")
                    msg.exec_()
                    self.stopMethod()
            
            self.time.append(self.endTime-self.startTime-self.offsetTime)
            
            if self.pidActive == True:
                self.pid.setpoint=int(self.flowCmdSent)
                self.pressure = int(self.pid(self.plotFlow[-1]))
                if self.stateCPLabel.text() == "Connected":
                    try:
                        self.continuousPump.set_pressure(self.pressure, hold='00:00:00:00')
                    except Exception as e: 
                        print(f"Error: {e}")
                        self.errorPort(self.comPortsCP.currentText())
                        self.stopMethod()
                else:
                    self.errorPort("pumps")
                self.plotPressD.append(self.setPressD)
                self.plotPressC.append(self.pressure)
            else:
                self.plotPressC.append(self.setPressC)  # Add value of the pressure of the Coninuous Phase Pump
                self.plotPressD.append(self.setPressD)
            
            self.plotFlowCmd.append(self.flowCmdSent)
            self.plotFlow.append(self.flowMeter.read(33, 0, propar.PP_TYPE_FLOAT))  # Add a reading from the flowmeter

            if self.csvCheckbox.isChecked():
                try: 
                    with open(f"{self.csvFilename.text()}.csv", 'a', newline='') as csvfile:
                            f = csv.writer(csvfile, delimiter=',')
                            f.writerow([self.time[-1],self.plotFlow[-1],self.flowCmdSent,
                                        self.plotPressC[-1],self.plotPressD[-1]])
                except Exception as e: 
                    print(f"Error: {e}")
                    msg = QMessageBox()
                    msg.setIcon(QMessageBox.Critical)

                    msg.setText(f"Error while writing in the CSV")
                    msg.setInformativeText("Please check the CSV file name")
                    msg.exec_()

            self.graphFlow.clear()
            self.graphPressure.clear()

            self.graphPressure.plot(self.time,self.plotPressC,pen='b')
            self.graphPressure.plot(self.time,self.plotPressD,pen='r')
        
            self.graphFlow.plot(self.time,self.plotFlow,pen='g')
            self.graphFlow.plot(self.time,self.plotFlowCmd,pen='r')

    
    def stopMethod(self):
        self.hasStop = True
        self.pidActive = False
        self.timer.stop()

    def refreshMethod(self):
        print("Refresh")
        self.comPortsCP.clear()
        self.comPortsDP.clear()
        self.comPortsFM.clear()
        self.testPump.clear()
        self.testFlow.clear()
        ports = serial.tools.list_ports.comports()

        for port, desc, hwid in sorted(ports):
                print("{}: {} [{}]".format(port, desc, hwid))
                self.comPortsCP.addItem(port)
                self.comPortsDP.addItem(port)
                self.comPortsFM.addItem(port)
                self.testPump.addItem(port)
                self.testFlow.addItem(port)

    def connectMethod(self):
        try:
            self.continuousPump = Py_P_Pump.P_pump(self.comPortsCP.currentText(), name='Continuous Phase Pump', pump_id=1, verbose=False)
            self.stateCPLabel.setText("Connected")
        except Exception as e: 
            print(f"Error: {e}")
            self.errorPort(self.comPortsCP.currentText())
        try:
            self.dispersedPump = Py_P_Pump.P_pump(self.comPortsDP.currentText(), name='Dispersed Phase Pump', pump_id=0, verbose=False)
            self.stateDPLabel.setText("Connected")
        except Exception as e: 
            print(f"Error: {e}")
            self.errorPort(self.comPortsDP.currentText())
        try:
            self.flowMeter = propar.instrument(self.comPortsFM.currentText())
            self.stateFMLabel.setText("Connected")
        except Exception as e: 
            print(f"Error: {e}")
            self.errorPort(self.comPortsFM.currentText())
    
    def disconnectMethod(self):
        self.pid.set_auto_mode = False
        try:
            self.continuousPump.set_idle()
            self.dispersedPump.set_idle()
            self.timer.stop()
        except Exception as e: 
            print(f"Error: {e}")
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)

            msg.setText(f"Trouble Disconnecting the Pumps")
            msg.setInformativeText("Please restart the script after saving your data")
            msg.exec_()
        
        self.continuousPump = None
        self.dispersedPump = None
        self.flowMeter = None

        self.stateCPLabel.setText("Disconnected")
        self.stateDPLabel.setText("Disconnected")
        self.stateFMLabel.setText("Disconnected")
    
    def testPressureMethod(self):
        try:
            testPumpDevice = Py_P_Pump.P_pump(self.testPump.currentText(), name='Continuous Phase Pump', pump_id=0, verbose=True)
            testPumpDevice.set_pressure(int(self.testPressure.currentText()), hold='00:00:00:10')
        except Exception as e: 
            print(f"Error: {e}")
            self.errorPort(self.testPump.currentText())
        del testPumpDevice
    
    def testFlowmeterMethod(self):
        try:
            testFlowMeter = propar.instrument(self.testFlow.currentText())
            flow_read = testFlowMeter.read(33, 0, propar.PP_TYPE_FLOAT)
            print(flow_read)
            self.testFlowResult.setText(str(flow_read))
            testFlowMeter = None
        except Exception as e: 
            print(f"Error: {e}")
            self.errorPort(self.testFlow.currentText())
    
    def setFlowMethod(self):
        try:
            self.pid.set_auto_mode = False
            self.flowCmdSent = int(self.flowCmd.text())
            self.pidActive = True
            self.pid.set_auto_mode(True, last_output=500)#self.plotPressC[-1])
        except Exception as e: 
            print(f"Error: {e}")
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)

            msg.setText(f"Flow value incorrect")
            msg.setInformativeText("Please use numbers in the flow value")
            msg.exec_()
        #Check if the pressures are correct
        try:
            self.setPressD = int(self.pressureDispersedFlow.text())
        except:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)

            msg.setText(f"Incorrect value of Pressure")
            msg.setInformativeText("Please verify that you only use numbers")
            msg.exec_()
        #Check if it can connect to the pumps
        if self.setPressD < 20 :
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)

            msg.setText(f"Pressure value too low")
            msg.setInformativeText("Please use only values above 20 mPa")
            msg.exec_()
        else:
            if self.stateDPLabel.text() == "Connected":
                try:
                    self.dispersedPump.set_pressure(self.setPressD, hold='00:00:00:00')
                except Exception as e: 
                    print(f"Error: {e}")
                    self.errorPort(self.comPortsDP.currentText())
            else:
                self.errorPort("pumps")
    
    def setPressMethod(self):
        self.pidActive = False
        self.pid.set_auto_mode = False
        #Check if the pressures are correct
        try:
            self.setPressD = int(self.pressureDispersed.text())
            self.setPressC = int(self.pressureContinuous.text())
        except:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)

            msg.setText(f"Incorrect value of Pressure")
            msg.setInformativeText("Please verify that you only use numbers")
            msg.exec_()
        #Check if it can connect to the pumps
        if self.setPressD < 20 or self.setPressC < 20:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)

            msg.setText(f"Pressure value too low")
            msg.setInformativeText("Please use only values above 20 mPa")
            msg.exec_()
        else:
            if self.stateCPLabel.text() == "Connected" and self.stateDPLabel.text() == "Connected":
                try:
                    self.dispersedPump.set_pressure(self.setPressD, hold='00:00:00:00')
                except Exception as e: 
                    print(f"Error: {e}")
                    self.errorPort(self.comPortsDP.currentText())
                try:
                    self.continuousPump.set_pressure(self.setPressC, hold='00:00:00:00')
                except Exception as e: 
                    print(f"Error: {e}")
                    self.errorPort(self.comPortsCP.currentText())
            else:
                self.errorPort("pumps")
    
if __name__ == '__main__':
    app = QApplication(sys.argv)
    clock = Window()
    clock.show()
    sys.exit(app.exec_())
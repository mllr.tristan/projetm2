# Import the propar module
import propar
import Py_P_Pump
import time
import csv
import time
import os.path
from simple_pid import PID
from PyQt5.QtCore import Qt,QTimer
from PyQt5 import QtWidgets
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
import sys  # We need sys so that we can pass argv to QApplication
import os



class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.target =400
        #Ziegler phase 1
        #self.pid = PID(1, 1.848, 0, setpoint=self.target)
        #Correct Ziegler 
        self.pid = PID(1, 1,1, setpoint=self.target)
        Ku = 8
        Tu = 8
        Ti = 0.5*Tu
        Td = 0.125*Tu
        Kp = 0.2*Ku
        Ki = (0.4*Ku)/Tu
        Kd = 0.066*Ku*Tu*1.5
        #self.pid = PID(Kp, Ki,Kd, setpoint=self.target)
        self.pid.output_limits = (30, 2500)

        # Assume we have a system we want to control in controlled_system

        self.el_flow = propar.instrument('COM8')
        self.red_pump = Py_P_Pump.P_pump('COM11', name='Red_Pump', pump_id=1, verbose=False)

        self.red_pump.set_idle()
        time.sleep(2)

        self.flow = self.el_flow.read(33, 0, propar.PP_TYPE_FLOAT)
        print(self.flow)
        self.graphWidget = pg.PlotWidget()
        self.setCentralWidget(self.graphWidget)

        self.start = time.time()

        self.plotTime =[]
        self.plotSetPoint =[]
        self.plotFlow = []
        self.plotPressure =[]

        self.timer = QTimer()
        self.timer.setInterval(500)
        self.timer.timeout.connect(self.plot)
        self.timer.start()
        print("Timer Started")
    
    def plot(self):
        if time.time()-self.start > 25:
            self.target = 600
            self.pid.setpoint= 600
        #print("Plot")
        # Compute new output from the PID according to the systems current value
        self.pressure = self.pid(self.flow)

        # Feed the PID output to the system and get its current value
        self.flow = self.el_flow.read(33, 0, propar.PP_TYPE_FLOAT)

        self.red_pump.set_pressure(int(self.pressure), hold='00:00:00:00')

        print(f"Target: {self.target} | Current: {self.flow} | Pressure: {self.pressure}")

        self.plotTime.append(time.time()-self.start)
        self.plotSetPoint.append(self.target)
        self.plotFlow.append(self.flow)
        self.plotPressure.append(self.pressure)

        self.graphWidget.clear()

        # plot data: x, y values
        self.graphWidget.plot(self.plotTime,self.plotSetPoint,name="Setpoint",pen='r')
        self.graphWidget.plot(self.plotTime,self.plotFlow,name="Flow",pen='b')
        self.graphWidget.plot(self.plotTime,self.plotPressure,name="Pressure",pen='g')

def main():
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()





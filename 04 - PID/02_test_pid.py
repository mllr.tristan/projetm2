# Import the propar module
import propar
import Py_P_Pump
import time
import csv
import time
import os.path
from simple_pid import PID

target =300

pid = PID(1, 0.1, 0.05, setpoint=target)
pid.output_limits = (25, 2000)

# Assume we have a system we want to control in controlled_system

el_flow = propar.instrument('COM8')
red_pump = Py_P_Pump.P_pump('COM12', name='Red_Pump', pump_id=1, verbose=False)

flow = el_flow.read(33, 0, propar.PP_TYPE_FLOAT)

while 1:
    # Compute new output from the PID according to the systems current value
    pressure = pid(flow)

    # Feed the PID output to the system and get its current value
    flow = el_flow.read(33, 0, propar.PP_TYPE_FLOAT)

    red_pump.set_pressure(int(pressure), hold='00:00:00:00')

    print(f"Target: {target} | Current: {flow} | Pressure: {pressure}")

    time.sleep(0.5)


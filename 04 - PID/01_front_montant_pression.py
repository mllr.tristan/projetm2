# Import the propar module
import propar
import Py_P_Pump
import time
import csv

# Connect to the local instrument, when no settings provided
# defaults to locally connected instrument (address=0x80, baudrate=38400)
el_flow = propar.instrument('COM8')

pump = 0

print(el_flow.read(33, 0, propar.PP_TYPE_FLOAT))

red_pump = Py_P_Pump.P_pump('COM12', name='Red_Pump', pump_id=1, verbose=True)

red_pump.set_idle()

start1 = time.time()
while time.time()-start1<5 :
    with open(f"01_front_montant.csv", 'a', newline='') as csvfile:
        f = csv.writer(csvfile, delimiter=',')
        f.writerow([time.time()-start1,pump,el_flow.read(33, 0, propar.PP_TYPE_FLOAT)])

pump = 500

red_pump.set_pressure(pump, hold='00:00:00:00')

start2 = time.time()
while time.time()-start2<10 :
    with open(f"01_front_montant.csv", 'a', newline='') as csvfile:
        f = csv.writer(csvfile, delimiter=',')
        f.writerow([time.time()-start1,pump,el_flow.read(33, 0, propar.PP_TYPE_FLOAT)])


red_pump.set_idle()